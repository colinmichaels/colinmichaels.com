var mix = require('laravel-mix');
var node = 'node_modules';

var paths = {
    'bootstrap'         : node + '/bootstrap',
    'jquery'            : node + '/jquery/dist',
    'jqueryEasing'      : node + '/jquery.easing',
    'clipboard'         : node + '/clipboard',
    'colorpicker'       : node + '/bootstrap-colorpicker/dist',
    'datepicker'        : node + '/bootstrap-datepicker/dist',
    'daterangepicker'   : node + '/bootstrap-daterangepicker',
    'datatables_bs'     : node + '/datatables.net-bs',
    'timepicker'        : node + '/bootstrap-timepicker',
    'fullcalendar'      : node + '/fullcalendar/dist',
    'slimscroll'        : node + '/jquery-slimscroll',
    'moment'            : node + '/bootstrap-daterangepicker',
    'select2'           : node + '/select2/dist',
    'fontawesome'       : node + '/font-awesome',
    'lity'              : node + '/lity/dist',
    'slick'             : node + '/slick-carousel/slick'
};

mix.copy(paths.fontawesome +'/fonts/*.*', 'public/fonts');

mix.js([
    paths.jquery        + '/jquery.js',
    paths.jqueryEasing  + '/jquery.easing',
    paths.bootstrap     + '/dist/js/bootstrap.bundle.js',
    paths.datatables_bs + '/js/dataTables.bootstrap.min.js',
    paths.lity          + '/lity.min.js',
    paths.slick         + '/slick.js',
    'resources/assets/js/app.js'
], 'public/js/all.js')
   .minify('public/js/all.js').sourceMaps();

mix.sass( paths.bootstrap + "/scss/bootstrap.scss", 'public/css')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass( paths.fontawesome + "/scss/font-awesome.scss", 'public/css')
   .minify('public/css/app.css').sourceMaps();

mix.styles([
        paths.lity + '/lity.min.css',
        paths.datatables_bs + '/css/dataTables.bootstrap.min.css',
        paths.slick         + '/slick.css',
        'public/css/font-awesome.css',
        'public/css/bootstrap.css'
    ],'public/css/vendor.css','./').minify('public/css/vendor.css');

mix.browserSync('colin.test');
