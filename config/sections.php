<?php

return [
    "intro"     => [
        "style"    => 'dark',
        "heading"  => "Colin Michaels",
        "sub-head" => "Web Developer",
        "content"  => " I have a broad background in everything from print media, web development, web design, and my current focus, which is software development. Over the last twenty years, I have worked in many different creative environments. They include web development, server administration, and audio engineering. Through these experiences, I have gained the skills which have helped me create successful and profitable ventures for my clients and past employers. I have developed hundreds of custom websites and also gone on to win three Grammy awards for my engineering works, along with many other successful projects."
    ],
    "about"     => [
        "style"   => "dark",
        "heading" => "About",
        "content" => ""
    ],
    "posts"     => [
        "style"   => "grey",
        "heading" => "Blog",
        "content" => ""
    ],
    "portfolio" => [
        "style"   => "dark",
        "heading" => "Work",
        "content" => nl2br("Here is a sampling of web development projects that I have had the extreme pleasure to work on. \nThrough this I have the opportunity to work besides amazingly talented teams of creatives.")
    ],
    "resume"    => [
        "style"   => "dark",
        "heading" => html_entity_decode("R&egrave;sum&egrave;"),
        "content" => nl2br("Creative Web Applications Developer with a proven ability to exceed client expectations.  Skilled in evaluating current and future technological needs in support of both short and long-term IT initiatives. Experienced in communicating effectively to cultivate excellent client relationships."),
        "link"    => "https://docs.google.com/document/d/1SPyAWI0K6riUW7YhsUO-Or18xt8vmzUx9fQCIRAwosY/edit?usp=sharing&authkey=CI_q2JQG"
    ],
    "contact"   => [
        "style"   => "dark",
        "heading" => "Contact",
        "content" => "Looking for a consultation or information feel free to contact me."
    ],
];
