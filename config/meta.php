<?php

return [
    'title' => 'Colin Michaels | Web Developer and technology fanatic',
    'description' => 'Colin Michaels is a creative web developer and applications developer in the South Florida.  He has over 20 years of combined experiences working on web development projects and applications.',
    'author' => 'Colin Michaels',
    'keywords' => 'Developer, Applications, Laravel, Wordpress, Javascript, HTML, jQuery, VueJS, Wed Design, Web Developer, Web Development, Applications Developer, Applications Development'
];
