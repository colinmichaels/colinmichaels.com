<div class="col-12" itemscope itemtype="https://schema.org/WebSite">
        <link itemprop="url" content="{{env('DOMAIN_REMOTE')}}"/>
        <form itemprop="potentialAction" itemscope
              itemtype="https://schema.org/SearchAction"
              method='get'
              action='{{route("blogetc.search")}}'
              class='text-center'>
            <meta itemprop="target" content="{{route("blogetc.search")}}?s={s}"/>
            <div class="input-group input-group-lg">

                <input itemprop="query-input" type='text' name='s' placeholder='Search...' class='form-control'
                       value='{{\Request::get("s")}}' aria-label="Search" aria-describedby="search-button" tab-index="1" required/>
                <div class="input-group-append">
                    <button id="search-button" type='submit' value='Search' class='btn btn-primary input-group-text' aria-label="Search"><i
                                class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </form>
</div>

