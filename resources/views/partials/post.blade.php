<div class="d-flex flex-column col-lg-4 col-md-6 col-sm-12 py-2 post" itemscope itemtype="http://schema.org/SocialMediaPosting">
    {!! $post->author->getSchema() !!}
    <a href="{{$post->url()}}" title="{{$post->title}}">
        <img itemprop="image" src="{{$post->image_url('large')}}" alt="{{$post->title}}" class="img-fluid gray-image">
    </a>
    <div class="d-flex flex-row flex-wrap flex-grow-1 flex-shrink-0  description py-2">

        <h3 itemprop="headline my-6">
            <a itemprop="url" href="{{$post->url()}}" title="{{$post->title}}" class="h-auto">
                {{ str_limit($post->title, 45) }}
            </a>
        </h3>

        <h4 class="sub-title py-2">{{$post->subtitle}}</h4>

        <p>{!! html_entity_decode($post->generate_introduction(150)) !!}</p>

        <div class="row align-self-end">
            <div class="col-md-8 col-sm-12 ">
                <a href="{{$post->url()}}" title="{{$post->title}}" class="btn btn-default">&#8984; INSPECT ></a>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="post-info">
                    <small class="badge badge-secondary">Posted: {{$post->posted_at->diffForHumans()}}
                        <meta itemprop="datePublished" content="{{$post->posted_at->format('Y-m-d')}}"/>
                    </small>
                </div>
            </div>
        </div>

    </div>

</div>
