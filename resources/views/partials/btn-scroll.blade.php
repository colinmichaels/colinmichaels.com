<a href="#{{($next??"")}}" title="{{($next??"")}}"
   class="btn btn-square scroll next align-self-end" data-target="#{{($next??"")}}">
    <i class="fa fa-angle-double-down animated"></i>
</a>
