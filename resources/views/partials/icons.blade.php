<link rel="apple-touch-icon" sizes="180x180" href="/images/icons/apple-touch-icon.png?v=476N2WlRXN">
<link rel="icon" type="image/png" sizes="32x32" href="/images/icons/favicon-32x32.png?v=476N2WlRXN">
<link rel="icon" type="image/png" sizes="16x16" href="/images/icons/favicon-16x16.png?v=476N2WlRXN">
<link rel="manifest" href="/images/icons/site.webmanifest?v=476N2WlRXN">
<link rel="mask-icon" href="/images/icons/safari-pinned-tab.svg?v=476N2WlRXN" color="#000000">
<link rel="shortcut icon" href="/images/icons/favicon.ico?v=476N2WlRXN">
<meta name="apple-mobile-web-app-title" content="Colin Michaels">
<meta name="application-name" content="Colin Michaels">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-config" content="/images/icons/browserconfig.xml?v=476N2WlRXN">
<meta name="theme-color" content="#000000">
