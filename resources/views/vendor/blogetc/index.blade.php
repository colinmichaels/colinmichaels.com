@extends("layouts.app",['title'=>$title])
@section("content")
    {{--https://webdevetc.com/laravel/packages/blogetc-blog-system-for-your-laravel-app/help-documentation/laravel-blog-package-blogetc#guide_to_views--}}
    <div class="container">
        <div class='row'>
            <div class='col-sm-12 blogetc_container'>
                @if(\Auth::check() && \Auth::user()->canManageBlogEtcPosts())
                    <div class="text-center">
                        <p class='mb-1'>You are logged in as a blog admin user.
                            <br>
                            <a href='{{route("blogetc.admin.index")}}'
                               class='btn border  btn-outline-primary btn-sm '>

                                <i class="fa fa-cogs" aria-hidden="true"></i>
                                Go To Blog Admin Panel</a>
                        </p>
                    </div>
                @endif
                    <section id="posts" class="section">
                        <div class="d-flex flex-column container">
                            <div class="d-flex flex-column">
                                <div class="section-header">
                                    <h3>{{config('sections.posts.heading')}}</h3>
                                    @if(isset($blogetc_category) && $blogetc_category)
                                        <h2 class='text-center'>{{$blogetc_category->category_name}}</h2>

                                        @if($blogetc_category->category_description)
                                            <p class='text-center'>{{$blogetc_category->category_description}}</p>
                                        @endif
                                    @endif
                                </div>
                             <div class="d-flex flex-row flex-wrap my-2">
                                        @forelse($posts as $post)
                                            @include('partials.post')
                                        @empty
                                            <div class='alert alert-danger'>No posts</div>
                                        @endforelse
                                    </div>

                                    <div class='col-sm-4'>
                                        {{$posts->appends( [] )->links()}}
                                    </div>
                            </div>
                        </div>
                    </section>
            </div>
        </div>
    </div>
@endsection
