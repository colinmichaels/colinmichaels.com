<div class="row">
    <div class="col-md-9">
        <h2 class='blog_title'>{{$post->title}}</h2>
        <h5 class='blog_subtitle'>{{$post->subtitle}}</h5>
        Posted @includeWhen($post->author,"blogetc::partials.author",['post'=>$post])
        <small class="badge badge-secondary">{{$post->posted_at->diffForHumans()}}</small>
        <?=$post->image_tag("large", false, 'img-fluid mx-auto'); ?>

        <p class="blog_body_content">
            {!!  $post->post_body_output() !!}

            {{--@if(config("blogetc.use_custom_view_files")  && $post->use_view_file)--}}
            {{--                                // use a custom blade file for the output of those blog post--}}
            {{--   @include("blogetc::partials.use_view_file")--}}
            {{--@else--}}
            {{--   {!! $post->post_body !!}        // unsafe, echoing the plain html/js--}}
            {{--   {{ $post->post_body }}          // for safe escaping --}}
            {{--@endif--}}
        </p>
    </div>
    <div class="col-md-2 ml-2 mt-5">
        <div class="row mt-5">
            @includeWhen($post->categories,"blogetc::partials.categories",['post'=>$post])
        </div>
        <div class="row">
            @include("vendor.blogetc.sitewide.recent_posts")
        </div>
    </div>

</div>

@if(\Auth::check() && \Auth::user()->canManageBlogEtcPosts())
    <a href="{{$post->edit_url()}}" class="btn btn-outline-secondary btn-sm pull-right float-right">Edit
        Post</a>
@endif


