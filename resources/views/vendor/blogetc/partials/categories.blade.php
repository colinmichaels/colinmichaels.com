<div class='btn-group-sm btn-group-vertical my-2'>
        <h5>Categories</h5>
    @foreach($post->categories as $category)
        <a class='btn btn-default btn-sm m-1' href='{{$category->url()}}'>
            {{$category->category_name}}
        </a>
    @endforeach
</div>
