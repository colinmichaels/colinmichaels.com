{{--Used on the index page (so shows a small summary--}}
{{--See the guide on webdevetc.com for how to copy these files to your /resources/views/ directory--}}
{{--https://webdevetc.com/laravel/packages/blogetc-blog-system-for-your-laravel-app/help-documentation/laravel-blog-package-blogetc#guide_to_views--}}

<div class="blog-post-loop">
    <div class='text-center'>
    <?=$post->image_tag("medium", true, ''); ?>
        </div>
    <div style='padding:10px;'>
    <h3 class=''><a href='{{$post->url()}}'>{{$post->title}}</a></h3>
    <h5 class=''>{{$post->subtitle}}</h5>
    <p>{!! html_entity_decode($post->generate_introduction(200)) !!}</p>
    <div class='text-left'>
        <a href="{{$post->url()}}" class="btn btn-default ">View Post</a>
    </div>
        </div>
</div>
