<div itemscope itemtype="https://schema.org/WebSite">
    <meta itemprop="url" content="{{env('DOMAIN_REMOTE')}}"/>
    <div class="col-12">
        <form method='get' action='{{route("blogetc.search")}}' class='text-center' itemprop="potentialAction" itemscope
              itemtype="https://schema.org/SearchAction">
            <meta itemprop="target" content="{{route("blogetc.search")}}?s={{\Request::get("s")}}"/>
            <div class="input-group input-group-sm">
                <input itemprop="query-input" type='text' name='s' placeholder='Search...' class='form-control'
                       value='{{\Request::get("s")}}' aria-label="Search" aria-describedby="search-button">
                <div class="input-group-append">
                    <button id="search-button" type='submit' value='Search' class='btn btn-primary input-group-text' aria-label="Search"><i
                                class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
