{{-- Intro Section --}}
<section id="intro" class="section section-{{config('sections.intro.style')}}">
    <div class="d-flex flex-column container">
        <div class="d-flex flex-column">
            <div class="section-header">
                <h1 class="brand-heading">{{config('sections.intro.heading')}}</h1>
                <h3 class="sub-heading pt-4">{{config('sections.intro.sub-head')}}</h3>
            </div>
            <p class="content">{{config('sections.intro.content')}}</p>
            @include('partials.btn-scroll',['next' => 'about'])
        </div>
</section>
