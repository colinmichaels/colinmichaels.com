{{-- Resume Section --}}
<section id="resume" class="section section-{{ config('sections.resume.style') }}">
    <div class="d-flex flex-column container">
        <div class="section-header">
            <h2>{{config('sections.resume.heading')}}</h2>
        </div>
        <p>
            {!! config('sections.resume.content') !!}
        </p>
        <a class="btn btn-default btn-lg col-md-4 col-sm-12 mb-5 mt-2 mx-auto"
           href="{{config('sections.resume.link')}}"
           title="Download a Copy of My Resume"
           class="external" target="_blank"> Download R&egrave;sum&egrave; <i
                    class="fa fa-cloud-download fa-fw"></i></a>
        <div class="section-header">
            <h3>Experience:</h3>
        </div>
        <div class="d-flex align-content-stretch flex-wrap ">
            @if($resume && $resume->experience)
                @foreach($resume->experience as $experience)
                    <div class="col-md-6 p-4 job">
                        <h5 itemprop="jobTitle">{{$experience->title}}
                            <br><span class="small"><a class="external" href="{{$experience->company->url}}"
                                                       title="{{$experience->company->title}}"
                                                       target="_blank">{{$experience->company->title}}  <i
                                            class="fa fa-external-link"></i></a></span></h5>
                        <small>{{$experience->company->city}}, {{$experience->company->state}}
                                                             | {{$experience->start_date}} - {{$experience->end_date}}</small>
                        @if($experience->responsibilities)
                            <ul class="pt-2">
                                @foreach($experience->responsibilities as $responsibility)
                                    <li>{{$responsibility}}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                @endforeach
            @endif
        </div>
        <div class="row">
            <div class="col-md-4">
                <h3>Education:</h3>
                <h6>Degrees:</h6>
                @if($resume && $resume->education)
                    @foreach($resume->education as $edu)
                        <div class="p-2">
                            <h4 class="small">{{$edu->degree}}</h4>
                            <h6 itemscope itemtype="http://schema.org/EducationalOrganization">
                                        <span itemprop="" itemscope
                                              itemtype="http://schema.org/CollegeOrUniversity"></span>
                                        <a href="{{$edu->url}}"
                                           title="{{$edu->title}}"
                                           rel="nofollow" target="_blank" itemprop="name">
                                            {{$edu->title}} <i class="fa fa-external-link"></i>
                                            <link itemprop="sameAs" href="{{$edu->wiki}}">
                                        </a>
                                        <br><span class="small" itemprop="address" itemscope
                                                  itemtype="http://schema.org/PostalAddress">
                                            <span itemprop="addressLocality">{{$edu->city}}</span>,
                                            <span itemprop="addressRegion">{{$edu->state}}</span>
                                            | {{$edu->start_date}} - {{$edu->end_date}}</span>
                            </h6>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="col-md-4">
                <h3>Proficiencies:</h3>
                <h6>Languages:</h6>
                <p class="small px-2">PHP, HTML5, CSS3/SCSS, JavaScript / JQuery/ VueJs</p>
                <h6>Programs:</h6>
                <p class="small px-2">PHP Storm, SQL Management Studio, Photoshop, Illustrator, Premiere, Microsoft Word</p>
                <h6>Platforms:</h6>
                <p class="small px-2"> Linux (Ubuntu /Centos) , Windows Server 2012/ 2008, Mac OSX</p>
            </div>
            <div class="col-md-4">
                <h3>Awards:</h3>
                <h6>Grammy Awards
                    <p class="small"> 2006 Latin Grammy | Calle 13</p></h6>
                <ul class="py-2 small">
                    <li>Best New Artist</li>
                    <li>Best Urban Music Album</li>
                    <li><a href="https://www.youtube.com/embed/vXtJkDHEAAc?autoplay=true"
                           title="Atrevete-te-te | Calle 13"
                           class="external" rel="nofollow" data-lity>Best Short Form Music Video for
                                                                     "Atrevete-te-te!"</a></li>
                </ul>
            </div>
        </div>
        @include('partials.btn-scroll',['next' => 'contact'])
</section>
