{{-- About Section --}}
<section id="about" class="collapsed section {{($section_class)??'section-light'}}">
    <div class="d-flex flex-column container">
        <div class="d-flex flex-column">
        <div class="section-header">
            <h2>{{config('sections.about.heading')}}</h2>
        </div>
        <p>I am a multi-faceted WEB APPLICATIONS DEVELOPER with degrees in both Computer Science and
           Information Technology as well as a degree in Audio Engineering. Over the last twenty
           years I have worked in many different creative capacities and markets. They include
           web/applications development, server administration, the music and entertainment business,
           collateral and print media, and audio and video mixing. Through these experiences, I have gained the
           skills, which have helped me create successful and profitable ventures for my clients and
           past employers. I have also been fortunate enough to win three Grammy awards for my engineering
           works, along with many other successful projects.</p>
        <p>I am presently focused on application development working with the Laravel Framework. I have been
           working along side my team at work on various <a href="https://wordpress.org/" target="_blank"
                                                            rel="nofollow" title="Wordpress"><i
                        class="fa fa-wordpress"></i> Wordpress</a> modifications including plugins and theme
           templates.</p>
        <p>I am
           versed in LAMP and WAMP development, I have an extensive background in database driven software
           design, server infrastructure and network security. I have earned my degrees in Engineering
           from Full Sail University with a degree in Information Technology and Computer Science
           degrees from Indian River State College.</p>
        </div>
        @include('partials.btn-scroll',['next' => 'posts'])
    </div>
</section>
