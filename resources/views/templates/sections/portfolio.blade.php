{{-- Work Section --}}
<section id="work" class="section section-{{config('sections.portfolio.style')}}">
    <div class="d-flex flex-column container">
        <div class="section-header">
            <h2>{{config('sections.portfolio.heading')}}</h2>
        </div>
        <p>
            {!! config('sections.portfolio.content') !!}
        </p>
        <div class="d-flex flex-row flex-wrap projects slick-slider" data-slick='{}'>
            @foreach($projects as $project)
                        <div class="my-2 d-flex flex-column flex-wrap justify-content-center project mx-auto">
                            <a class="p-2" href="{{$project->image}}" title="{{$project->url}}" data-lity>
                                <img class="image" alt="{{$project->title}}" src="{{$project->thumb}}"/>
                            </a>
                            <small  class="description mx-auto">{{$project->description}}</small>
                            <a href="{{$project->url}}"
                               target="_blank" rel="nofollow noindex"
                               title="Goto: {{$project->url}}">
                                <h5 class="text-center">
                                    Visit Site <i class="fa fa-external-link-square small"></i>
                                </h5>
                                <small>{{$project->decription}}</small>
                            </a>
                        </div>
            @endforeach
        </div>
        @include('partials.btn-scroll',['next' => 'resume'])
    </div>
</section>

