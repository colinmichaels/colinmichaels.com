{{-- Contact Section --}}
<section id="contact" class="section {{config('sections.contact.style')}}">
    <div class="d-flex flex-column container">
        <div class="section-header">
            <h2>{{config('sections.contact.heading')}}</h2>
        </div>
        <div class="d-flex flex-column justify-content-center">
            @if(!isset($message))
                <p>{{config('sections.contact.content')}}</p>
                {!! Form::open(array('route' => array('home.store'), 'method' => 'POST', 'id' => 'contact', 'class' => 'form')) !!}
                    <div class="d-flex flex-column">
                            <label class="p-2" for="name">
                            <input type="text" class="form-control" id="name" name="name"
                                   placeholder="Name">
                            </label>
                            <label class="p-2" for="email">
                            <input type="email" class="form-control" id="email" name="email"
                                   placeholder="Email">
                            </label>
                            <label class="p-2" for="name">
                            <textarea class="form-control" id="comment" name="comment"
                                      placeholder="Comments"></textarea>
                            </label>
                            <label class="pt-4" for="submit">
                                <button type="submit" name="submit" class="btn btn-default pull-right">Submit <i
                                            class="fa fa-paper-plane"></i></button>
                            </label>
                    </div>
                {!! Form::close() !!}
            @else
                <div class="overlay-message">
                    <h3>{{$message}}</h3>
                </div>
            @endif
        </div>
    </div>
</section>
