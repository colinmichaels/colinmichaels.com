@if($posts)
    <section id="posts" class="section section-{{config('sections.posts.style')}}">
        <div class="d-flex flex-column container">
            <div class="d-flex flex-column">
                <div class="section-header">
                    <h2>{{config('sections.posts.heading')}}</h2>
                </div>
                <div class="d-flex flex-row flex-wrap my-2">
                    @foreach($posts as $post)
                         @include('partials.post')
                    @endforeach
                </div>
                @include('partials.btn-scroll',['next' => 'work'])
            </div>
    </section>
@endif
