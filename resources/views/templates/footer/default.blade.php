<div class="container">
    <div class="d-flex flex-row justify-content-center p-2 py-sm-0  mx-auto">
        <a class="p-2 scroll" href="/#about"       target="_self"  title="About Colin">About</a>
        <a class="p-2 scroll" href="/#work"        target="_self"  title="Portfolio">Work</a>
        <a class="p-2 scroll" href="/#resume"      target="_self"  title="Resume">R&egrave;sum&egrave;</a>
        <a class="p-2 scroll" href="/pay"          target="_self"  title="Pay">Pay</a>
        <a class="p-2 scroll" href="/#contact"     target="_self"  title="Contact">Contact</a>
    </div>
    <div class="d-flex flex-row justify-content-center p-2 mx-auto">
        <a class="p-2" href="https://twitter.com/colinmichaels"  rel="nofollow" target="_blank" title="Twitter">
            <i class="fa fa-twitter fa-fw fa-2x"></i>
        </a>
        <a class="p-2" href="https://github.com/ColinMichaels" rel="nofollow" target="_blank"  title="GitHub">
            <i class="fa fa-github fa-fw fa-2x"></i>
        </a>
        <a class="p-2" href="https://www.instagram.com/colinmichaels/" rel="nofollow" target="_blank"  title="Instagram">
            <i class="fa fa-instagram fa-fw fa-2x"></i>
        </a>
        <a class="p-2" href="  https://www.linkedin.com/in/colinmichaels/" target="_blank"  title="Linkedin">
            <i class="fa fa-linkedin fa-fw fa-2x"></i>
        </a>
    </div>
    <div class="d-flex justify-content-center p-2">
            <p class="small text-center">Copyright &copy;{{date('Y')}} <a href="{{secure_url('/')}}" title="{{env('APP_NAME')}}">{{env('APP_NAME')}}</a></p>
    </div>
</div>
