<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand scroll" title="Colin Michaels" href="{{url('/#colin-michaels')}}"><span>CM</span>  <small>| web<em>(</em><span>developer</span><em>)</em></small></a>
        <div class="navbar-toggler" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
                <div class="hamburger collapsed" data-toggle="collapse"  data-target="#navbarResponsive">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarResponsive"  data-offset="0">
            <ul id="main-nav" class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link scroll" title="About" href="{{url('/#about')}}">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link scroll" title="Blog" href="{{url('blog')}}"><i class="fa fa-file"></i> <span class="s-sm-none">Blog</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link scroll" title="Portfolio" href="{{url('/#work')}}"><i class="fa fa-briefcase"></i> <span class="s-sm-none">Work</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link scroll" title="Resume" href="{{url('/#resume')}}"><i class="fa fa-briefcase"></i> <span class="s-sm-none">Resume</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link scroll" title="Contact" href="{{url('/#contact')}}"><i class="fa fa-envelope"></i> <span class="s-sm-none contact">Contact</span></a>
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0)" class="nav-link search-toggle" data-toggle="collapse" data-target="#sub-nav"><i class="fa fa-search"></i> <span class="s-sm-none">Search</span></a>
                </li>
                @auth
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <i class="fa fa-user"></i> {{ Auth::user()->initials }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <a class="dropdown-item" href="{{route("blogetc.admin.index")}}" title="blog admin">Blog Admin</a>

                                    <a class="dropdown-item" href="{{route('projects.index')}}" title="projects">Projects</a>

                            <a class="nav-link" title="Pay" href="{{route('Pay')}}"><i class="fa fa-money"></i> <span class="visible-sm-block">Pay</span></a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endauth
            </ul>
        </div>
    </div>
</nav>
<div class="sub-nav fixed-top collapse" id="sub-nav">
    <div class="container">
            @include('partials.search_form')
    </div>
</div>
