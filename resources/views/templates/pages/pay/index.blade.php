@extends('layouts.app')
@push('header_scripts')
@endpush
@section('navigation')
    @include('templates.navigation.default')
@endsection
@section('content')
    <section id="payment" class="section">
        <div class="container">
            <div class="row section">
                <div class="col-lg-12 mx-auto">
                    <FORM action="https://www.paypal.com/cgi-bin/webscr" class="form-horizontal" method="post">
                        <fieldset>
                            <!-- Form Name -->
                            <h4 class="text-center">Payment for Services Rendered</h4>
                            <!-- Text input-->
                            <div class="form-group">
                                <div class="row justify-content-md-center">
                                    <div class="col-sm-6">
                                        <label class="col-form-label col-form-label-sm" for="textinput">Name</label>
                                        <input id="textinput" name="textinput" type="text" placeholder="your name"
                                               class="form-control input-md" required="required">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-form-label col-form-label-sm" for="business">Email</label>

                                        <input id="business" name="business" type="text" placeholder="PayPal Email"
                                               class="form-control input-md" required="required">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-form-label col-form-label-sm" for="invoice">Invoice Number</label>

                                        <input id="invoice" name="invoice" type="number" placeholder="######"
                                               class="form-control input-md">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-form-label col-form-label-sm" for="phone">Phone</label>

                                        <input id="phone" name="phone" type="text" placeholder="xxx-xxx-xxxx"
                                               class="form-control input-md">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-form-label col-form-label-sm" for="exampleInputAmount">Amount (in
                                            dollars)</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" class="form-control" id="amount" name="amount"
                                                   placeholder="Amount" required="required">
                                            <div class="input-group-addon">.00</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 mx-auto">
                                        <label class="col-form-label col-form-label-sm" for="notes">Notes</label>
                                        <textarea class="form-control" id="notes" name="notes">Your Comments</textarea>
                                    </div>
                                </div>


                            </div>
                            <INPUT TYPE="hidden" name="item_name" value="Development Services">
                            <INPUT TYPE="hidden" name="charset" value="utf-8">
                            <INPUT TYPE="hidden" NAME="return" value="{{ url('/pay') }}">
                            <!-- Saved buttons use the "secure click" command -->
                            <input type="hidden" name="cmd" value="_xclick">

                            <!-- Saved buttons display an appropriate button image. -->
                            <div class="form-group">
                                <label class="col-form-label col-form-label-sm" for="exampleInputAmount">Click to Pay with
                                    PayPal</label>
                                <button type="submit" class="btn btn-primary" name="submit" border="0"
                                        title="PayPal - The safer, easier way to pay online"><i class="fa fa-paypal"></i> Pay </button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection
