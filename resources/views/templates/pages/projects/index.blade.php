@extends('layouts.app')
@push('header_scripts')
@endpush
@section('navigation')
    @include('templates.navigation.default')
@endsection
@section('content')
    <div class="container mt-4">
        <div class="row">
            <div class="col-lg-12 mx-auto pt-4 mt-4">
                <section id="projects" class="section">
                    <div class="container">
                        <table class="table datatable table-striped table-sm" data-page-length="20">
                            <thead>
                            <th>ID</th>
                            <th>Title</th>
                            <th>URL</th>
                            <th>Description</th>
                            <th>Image</th>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                                <tr>
                                    <td>{{$project->id}}</td>
                                    <td>{{$project->title}}</td>
                                    <td>{{$project->url}}</td>
                                    <td>{{$project->description}}</td>
                                    <td><img class="img-thumbnail" src="{{$project->thumb}}"/></td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
