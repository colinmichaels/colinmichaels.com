@extends('layouts.app')
@push('header_scripts')
@endpush
@section('navigation')
@endsection
@section('content')
    @include('templates.sections.intro')
    @include('templates.sections.about')
    @include('templates.sections.posts',['section_class' => 'section-dark'])
    @include('templates.sections.portfolio')
    @include('templates.sections.resume')
    @include('templates.sections.contact')
@endsection
@push('footer_scripts')
@endpush
