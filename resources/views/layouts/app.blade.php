<!DOCTYPE html>
<html lang="en">
<head>
    {{--  Google Tag Manager --}}
    <script>(function (w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start':
					new Date().getTime(), event: 'gtm.js'
			});
			var f                          = d.getElementsByTagName(s)[0],
				j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async                        = true;
			j.src                          =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', '{!! config('analytics.tag_manager_id') !!}');</script>
    {{--  End Google Tag Manager --}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {{--   CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="all"/>
    <title>{{ (Request::route()->getName() != "/")? ucfirst(Request::route()->getName())." | ".config('app.name') : config('meta.title') }}</title>
    <meta name="description" content="{{config('meta.description')}}">
    <meta name="author" content="{{config('meta.author')}}">
    <meta name="keywords" content="{{config('meta.keywords')}}">
    <link rel="canonical" href="{{url()->full()}}">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="{{mix('/css/vendor.min.css')}}" rel="stylesheet">
    <link href="{{mix('/css/app.min.css')}}" rel="stylesheet">
    @include('partials.icons')
    @if(isset($schema))
        {!! $schema !!}
    @endif
    @if(isset($page))
        {!! $page->openGraph() !!}
    @endif
    {!!\WebDevEtc\BlogEtc\Helpers::rss_html_tag()!!}
    {{--<!-- Scripts -->--}}
    @stack('header_scripts')
</head>
<body id="colin-michaels">
{{-- Google Tag Manager (noscript) --}}
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-Q6BN"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
{{-- End Google Tag Manager (noscript) --}}
<div id="wrapper">
    <div class="overlay"></div>
    @include('templates.navigation.default')
    @yield('navigation')
    <main id="app" data-spy="scroll" data-target="#main-nav" data-offset="0">
        @yield('content')
    </main>
    <footer>
        @include('templates.footer.default')
    </footer>
</div>
<script type="text/javascript" src="{{ mix('/js/all.min.js') }}" async defer></script>
@stack('footer_scripts')
</body>
</html>
