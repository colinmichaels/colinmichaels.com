/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


//Vue.component('example-component', require('./components/ExampleComponent.vue'));
// const app = new Vue({
//     el: '#app'
// });
function clickableRow() {
	$('.clickable').contextmenu(function (event) {
		event.preventDefault();
		window.location.href = $(this).data('href');
		return false;
	});
}

function jsUcfirst(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

(function ($) {

	"use strict"; // Start of use strict
	var hash       = document.location.hash.replace('#', '');
	var page_title = document.title;
	if (!page_title.includes(hash)) {
		document.title = page_title + " | " + jsUcfirst(hash);
		// history.push(null,null,document.location);
	}

	$('.overlay').fadeOut();

	$(".slick-slider").slick(
		{
			autoplay:true,
			speed: 1200,
			slidesToShow: 4,
			swipeToSlide: true,
			arrows :false,
			dots:false,
			responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					infinite: true
				}

			}, {

				breakpoint: 600,
				settings: {
					slidesToShow: 2
				}

			}, {

				breakpoint: 300,
				settings: "unslick" // destroys slick

			}]
		});

	$('table.datatable').DataTable({
		stateSave: true,
		stateSaveCallback: function (settings, data) {
			localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
			clickableRow();
		},
		stateLoadCallback: function (settings) {
			return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
			clickableRow();
		}
	});
	// Smooth scrolling using jQuery easing
	$('a.scroll[href*="#"]:not([href="#"])').click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target     = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: (target.offset().top - 48)
				}, 1000, "easeInOutExpo");
				return false;
			}
		}
	});
	// Closes responsive menu when a scroll trigger link is clicked
	$('.scroll').click(function () {
		$('.navbar-collapse').collapse('hide');
	});
	// Activate scrollspy to add active class to navbar items on scroll
	// $('body').scrollspy({
	//     target: '#mainNav',
	//     offset: 154
	// });
	// Collapse Navbar
	var navbarCollapse = function () {
		var main_nav = $("#mainNav");
		if (main_nav.offset().top > 200) {
			main_nav.addClass("navbar-shrink")
					.find('a span').first()
					.text('CM');
		} else {
			main_nav.addClass("navbar-shrink")
					.find('a span').first()
					.text('Colin Michaels');
		}
	};
	// Collapse now if page is not at top
	// navbarCollapse();
	// Collapse the navbar when page is scrolled
	$(window).scroll(navbarCollapse);
})(jQuery); // End of use strict

