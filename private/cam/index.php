<?php session_start();
// Store Session Data
$url = "http://colinmichaels.dyndns.org";
//$user = urlencode($_SERVER['PHP_AUTH_USER']);
$user = 'admin';
$_SESSION['login_user'] = $user;  // Initializing Session with value of PHP Variable
//$pass =  urlencode($_SERVER['PHP_AUTH_PW']);
$pass = 'dikia3540';
$cams = array("200", "500"); // cam ports
?>
<html>
<head>
    <meta charset="utf-8">
    <title id="pageTitle">Colin's Hurricane Central (IRMA 2017)</title>
    <meta property="og:title" content="Colin's Hurricane Central (IRMA 2017)"/>
    <meta property="og:description"
          content="A View from hurricane IRMA from Jupiter and Stuart Florida.  Set this up temporarily for all my friends and family memebers to get a small glimpse as to what is happening during the storm.  There is limited availabilty for the camera images so please do not share this with the public."/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="http://colinmichaels.com/cam/"/>
    <meta property="fb:app_id" content="112752895476149"/>
    <meta property="og:image" content="http://colinmichaels.com/cam/img/preview.jpg"/>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/ekko-lightbox.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-13296389-8', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <h1>Colin's Hurricane Central (Hurricane IRMA 2017) </h1>
         <h1 class="text-danger">Power is still out in most places. Everyone is safe so far limited damage. Thanks for visiting</h1>
<!--        <h4>Jupiter Florida</h4>-->
<?php
//        $cols = round(8 / count($cams));
//        foreach ($cams as $cam) {
//            echo '<div class="col-md-' . $cols . ' col-sm-12">';
//            echo '<img id="' . $cam . '" class="img-responsive cam"/>';
//            echo '</div>';
//        }
//        ?>

<!--        <div class="col-md-4 cam">-->
<!--            <h4>Stuart Florida Manatea Pocket</h4>-->
<!---->
<!--            <div class="embed-responsive embed-responsive-16by9 ">-->
<!--                <iframe class="embed-responsive-item"-->
<!--                        src="http://goldendreams.viewnetcam.com:50001/nphMotionJpeg?Resolution=640x480&amp;Quality=Clarity"-->
<!--                        frameborder="0"></iframe>-->
<!--            </div>-->
<!--        </div>-->
        <div class="col-md-4">
            <h4>Jupiter Inlet Cam (click to play)</h4>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="http://www.evsjupiter.com/stream.htm"
                        frameborder="0"></iframe>
            </div>
        </div>
        <div class="col-md-4">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item"
                        src="https://embed.windy.com/embed2.html?lat=26.912&lon=-80.101&zoom=8&type=radar&location=coordinates&marker=&overlay=radar"
                        frameborder="0"></iframe>
            </div>
        </div>

        <div class="col-md-4">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item"
                        src="https://google.org/crisismap/2017-irma?hl=en&llbox=29.242%2C24.635%2C-74.498%2C-83.233&t=TERRAIN&layers=3%3A98%2C1340721332252%3A96%2C30%2C1%2C31%2C32%2C5%2C49%2C15%2C11%2C20%2C12%2Clayer9%2Clayer8&embedded=true"></iframe>
            </div>
        </div>

    </div>
    <div class="panel panel-default hide">
        <div class="panel-heading">
            <small><strong>Cam Controls</strong> - <i class="glyphicon glyphicon-time"></i><span id="time"></span>
            </small>
            <span class="pull-right"><a href="#" data-toggle="collapse" data-target="#controls"><i
                            class="glyphicon glyphicon-arrow-down"></i> </span></div>
        <div class="panel-body collapse" id="controls">
            <div class="row">
                <div class="col-md-6 col-sm-12 ">
                    <div class="input-group" role="group">
                        <select id="cur_cam" title="Select Camera" class="form-control">
                            <?php
                            foreach ($cams as $cam) {
                                echo '<option value="' . $cam . '">' . $cam . '</option>';
                            }
                            ?>
                        </select>
                        <span class="input-group-addon">
								<input type="checkbox" class="orientation" data-param="5" data-mult="1"
                                       title="flip camera" aria-label="Flip Cam">
								<i class="glyphicon glyphicon-resize-vertical"></i>
							</span>
                        <span class="input-group-addon">
								<input type="checkbox" class="orientation" data-param="5" data-mult="2"
                                       title="mirror camera" aria-label="Mirror Cam">
								<i class="glyphicon glyphicon-resize-horizontal"></i>
							</span>
                        <span class="input-group-addon">
								<input type="checkbox" id="ir" title="IR" aria-label="Infared">
								<i class="glyphicon glyphicon-sunglasses"></i>
							</span>
                        <span class="input-group-btn">
							<button class="btn btn-default cam_control" data-cntl="6" title="left"><i
                                        class="glyphicon glyphicon-arrow-left"></i></button>
							<button class="btn btn-default cam_control" data-cntl="0" title="up"><i
                                        class="glyphicon glyphicon-arrow-up"></i></button>
							<button class="btn btn-default cam_control" data-cntl="2" title="down"><i
                                        class="glyphicon glyphicon-arrow-down"></i></button>
							<button class="btn btn-default cam_control" data-cntl="4" title="right"><i
                                        class="glyphicon glyphicon-arrow-right"></i></button>
							<button class="btn btn-default cam_control" data-cntl="25" title="center"><i
                                        class="glyphicon glyphicon-screenshot"></i></button>
							</span>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <label for="presets">Presets Call:</label>
                    <div class="btn-group btn-group-xs" id="presets" name="presets" data-toggle="buttons">
                        <?php for ($i = 1; $i <= 5; $i++) {
                            echo '<label class="btn btn-default">
			<input type="radio" class="presets" name="presets" data-preset="' . $i . '" autocomplete="off"> ' . $i . '
		  </label>';
                        }
                        ?>
                    </div>
                    <label for="presets">Presets Set:</label>
                    <div class="btn-group btn-group-xs" id="presets" name="presets" data-toggle="buttons">
                        <?php for ($i = 1; $i <= 5; $i++) {
                            echo '<label class="btn btn-default">
			<input type="radio" class="presets-set" name="presets" data-preset="' . $i . '" autocomplete="off"> ' . $i . '
		  </label>';
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <form class="form">
                        <div class="form-group">
                            <label for="bright"><i class="glyphicon glyphicon-eye-open"></i> Brightness:</label>
                            <input class="cam_params" class="form-control" type="range" name="bright" id="bright"
                                   data-param="1" value="128" step="10" min="0" max="255">
                            <label for="contrast"><i class="glyphicon glyphicon-adjust"></i> Contrast:</label>
                            <input class="cam_params" class="form-control" type="range" name="contrast" id="contrast"
                                   data-param="2" value="128" step="10" min="0" max="255">
                            <label for="timeout">Refresh Rate <i class="glyphicon glyphicon-refresh"></i></label>
                            <select id="timeout" class="form-control">
                                <?php for ($i = 1000; $i <= 10000; $i += 1000) {
                                    echo "<option value='" . $i . "'>" . $i . "</option>";
                                }
                                ?>
                            </select>
                            <label for="framerate">Frame Rate <i class="glyphicon glyphicon-film"></i></label>
                            <select id="framerate" data-param="6" class="form-control cam_params">
                                <?php for ($i = 5; $i <= 30; $i += 5) {
                                    echo "<option value='" . $i . "'>" . $i . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </form>
                    <a class="btn btn-default  btn-lg open_page" data-page="" target="_blank"><i
                                class="glyphicon glyphicon-camera"></i></a>
                    <a class="btn btn-default  btn-lg open_page" data-page="get_camera_params.cgi" target="_blank"><i
                                class="glyphicon glyphicon-text-size"></i></a>
                    <input type="file" class="btn btn-default" capture="camera" accept="image/*" id="cameraInput"
                           name="cameraInput">

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <code id="debug"> </code>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/ekko-lightbox.min.js"></script>
<?php include_once('js/main.php'); ?>
</body>
</html>