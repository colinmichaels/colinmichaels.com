
		var timeout = 5000;
		$("#timeout").val(timeout);
		var cur_cam = $("#cur_cam").val();
		<?php echo "var cams = ". json_encode($cams) . ";\n"; ?>
		function update_cams()
		{
			 var d = $.now();
			$.each(cams, function(i,val) {
				$("#"+val).attr('src','<?php echo $url.':';?>'+val+'/snapshot.cgi?user=<?php echo $user.'&pwd='.$pass; ?>&time='+d );
			});
			var time = new Date(d);
			$("#time").text(time);
			setTimeout(update_cams,timeout);
		}
		update_cams();
		function change_params(param,val)
		{
			$.get('<?php echo $url.':';?>'+cur_cam+'/camera_control.cgi?user=<?php echo $user.'&pwd='.$pass; ?>&param='+param+'&value='+val );
			debug("Function:change_params\nParam:"+param+"\nVal:"+val);
		}
		function move_cam(param,val)
		{
				$.get('<?php echo $url.':';?>'+cur_cam+'/decoder_control.cgi?user=<?php echo $user.'&pwd='.$pass; ?>&command='+param+'&onestep='+val );
				debug("Function:move_cam\nParam:"+param+"\nVal:"+val);
		}
		function get_params()
		{
			$.ajax({
				xhrFields: {
					withCredentials: false
				},
				type :"GET",
				 password : "<?php echo $pass; ?>",
				username : "<?php echo $user; ?>",
				contentType: 'text',
				crossDomain: true,
				url: '<?php echo $url; ?>:'+cur_cam+'/get_camera_params.cgi',
				//url: 'get_params.php',
				data : {user : "<?php echo $user; ?>", pass: "<?php echo $pass; ?>",cam : cur_cam ,url : "<?php echo $url; ?>"},
				success: function (response) {
						debug(response);//your success code
					},
					 error: function (xhr, ajaxOptions, thrownError) {
						debug(xhr.status+":"+thrownError);//your error code
					}
			});
		}
		get_params();
		function gotPic(event) {
        if(event.target.files.length == 1 && 
           event.target.files[0].type.indexOf("image/") == 0) {
            $("#yourimage").attr("src",URL.createObjectURL(event.target.files[0]));
        }
		}
		function debug(e)
		{
			$("#debug").text(e);
		}
		$("#timeout").change(function(){
			timeout = $(this).val();
			debug("Refresh: "+timeout);
		});
		$(".presets").change(function(){
			var val = $(this).data('preset');
			val = (val*2)+29;
			move_cam(val,0);
		});
		$(".presets-set").change(function(){
			var val = $(this).data('preset');
			val = (val*2)+28;
			move_cam(val,0);
		});
		$("#cur_cam").change(function(){
			cur_cam = $("#cur_cam").val();
			$('.cam').each(function(){
				$(this).css('border','none');	
			});
			$("#"+cur_cam).css('border','2px blue solid');
		});
		$(".orientation").change(function(){
			var param = $(this).data('param');
			var mult = $(this).data('mult');
			if($(this).is(':checked'))
			val = mult;  else val =0;
			change_params(param,val);
		});
		$("#ir").change(function(){
			var param = 14;
			var val = 0;
			if($(this).is(':checked'))
			val = 1;  else val =0;
			change_params(param,val);
		});
		$(".cam_control").click(function(){
			var param = $(this).data('cntl');
			move_cam(param,0);
		});
		$(".cam_params").change(function(){
			var param = $(this).data('param');
			var val = $(this).val();
			change_params(param,val);
		});
		$('.cam').click(function(e){
			cur_cam = $(this).attr('id');
			$('.cam').each(function(){
				$(this).css('border','none');	
			});
			$(this).css('border','2px blue solid');
			$("#cur_cam").val(cur_cam);
			get_params();
		});
		$('.cam').dblclick(function(e){
			cur_cam = $(this).attr('src');
			$('modal').find('.modal-content').html('<img class="img img-responsive" src="'+cur_cam+'"/>');
			$('.modal').modal({
			  keyboard: false
			});
		});
		$(".open_page").click(function(){
			var page = $(this).data('page');
			window.open("<?php echo $url; ?>"+":"+cur_cam+"/"+page);
		});
		$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
				event.preventDefault();
				$(this).ekkoLightbox();
			}); 
