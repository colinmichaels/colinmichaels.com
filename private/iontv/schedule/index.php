<?php date_default_timezone_set('America/New_York');
/* 
Developed By: Colin Michaels
EMAIL: colinmichaels@gmail.com
PURPOSE: ION TV - programming review

*/
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Ion Television Schedule - Week Of <?php echo $date; ?></title>
	 <!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/style.css" >
</head>
 <body>
<div id="wrapper">
	<div class="row-fluid top-bar" data-spy="affix" data-offset-top="100">
		<?php include_once("inc/nav.php"); ?>
	</div>	
	<div class="row-fluid sub-nav darker-bg" data-spy="affix" data-offset-top="140">
				<div class="container hidden-xs" id="info-bar">
					<div class="col-xs-12 col-md-9">
						<ul class="list-inline">
							<li class="show-title">SCHEDULE: <small class='text-muted' id="cur_time"></small></li>
						</ul>
					</div>
					<div class="col-xs-12 col-md-3 text-right social">
						<ul class="list-inline">
							<li><a data-toggle="tooltip" href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
							<li><a data-toggle="tooltip" href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
							<li><a data-toggle="tooltip" href="#" title="Instagram"><i class="fa fa-instagram"></i></a></li>
							<li><a data-toggle="tooltip" href="#" title="Search"><i class="fa fa-search"></i></a></li>
							<li><a data-toggle="tooltip" href="#" title="Local"><i class="fa fa-map-marker"></i></a></li>
							<li><a data-toggle="tooltip" href="#" title="User"><i class="fa fa-user"></i></a></li>
						</ul>
					</div>
				</div>		
	</div>
  	<div class="row-fluid text-center" id="top_schedule" data-spy="affix" data-offset-top="150">	
			<div class="container hidden-xs" >
				<div class="col-xs-2 col-sm-1">
					<em class="fa fa-angle-left fa-3x prev-week  disabled "></em>
				</div>
				<div class="days_select">
				</div>
				<div class="col-xs-2 col-sm-1">
					<em class="fa fa-angle-right fa-3x next-week "></em>
				</div>
			</div>
	</div>
  <div class="container-fluid" id="main">
		<div class="row" id="content">
		<div class="panel-group schedule" id="accordion" role="tablist" aria-multiselectable="false">
		</div>
		</div>
  </div>
</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script> 
<script src="js/moment/moment.min.js"></script>
<script src="js/main.js?v0.0.1"></script>
</body>
  </html>