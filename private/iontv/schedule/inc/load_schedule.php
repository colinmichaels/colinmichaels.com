<?php
$api_url = "https://dev-api.iontelevision.com/1.0/schedule";
function get_data($url ,$fields) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	curl_setopt($ch, CURLOPT_POST, true); 
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}

function prepare_params($params)
{
	foreach($params as $key=>$value) {
            $fields_string .= $key.'='.$value.'&';
    }
}
function clean_str($str){ return filter_var($str, FILTER_SANITIZE_STRING); }

if(isset($_REQUEST['date']))
	{
		echo json_encode(get_data($api_url,$_REQUEST));
	}
else die("failed");
	
?>