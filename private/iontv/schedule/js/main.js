//! main.js
//! version : 0.0.1
//! author : Colin Michaels
function load_schedule(date)
{
	$.ajax({
		method: "GET",
		url: "inc/load_schedule.php",
		dataType: 'json',
		data: { date : date  }
		}).done(function(data){
			var q = $.parseJSON(data);
			var featured = q.featured;
			$('.schedule').html("");
			$.each(JSON.parse(data), function(index, element) {

				if($.isArray(element)){
					$.each(element,function(i,e)
					{
						var panelback = 'dark-bg';
						if(i % 2 == 0)panelback ='light-bg'; 
						$(".schedule").append(
						"<div class='panel panel-default schedule-item' data-show-id='"+e.showID+"'>"+
						"<div class='panel-heading' role='tab' id='phead-"+e.timestamp+"'></div>"+
						"<div class='panel-title "+panelback+"' data-toggle='collapse' data-parent='#accordion' href='#program-"+e.timestamp+"' aria-expanded='false' aria-controls='program-"+e.timestamp+"'>"+
							"<div class='container'>"+
								"<div class='row'>"+
								"<div class='col-md-3 col-xs-3'>"+
									"<h2>"+show_times(e.date_iso_8601,e.time_zone)+"</h2>"+
								"</div>"+
								"<div class='col-md-9 col-xs-9'>"+
								"<strong>"+e.showName+"</strong>"+
								"<p><small>"+e.episodenumber+"</small>: "+e.episodetitle.toUpperCase()+
								"<span><em class='drop-icon fa fa-angle-down fa-pull-right fa-3x'></em></span></p>"+
								"</div></div></div></div>"+
								"<div id='program-"+e.timestamp+"' data-isotime='"+moment(e.date_iso_8601).format('MM-DD-HH')+"' class='panel-collapse collapse' role='tabpanel' aria-labelledby='phead-"+e.timestamp+"'>"+
									"<div class='panel-body'>"+
										"<div class='container'>"+
											"<div class='row'>"+
												"<div class='col-xs-12 col-sm-4'>"+
												//episode thumbnail
												"<div class='video-thumbnail'>"+
												"<a href='"+e.link+"'>"+
												"<img class='img img-responsive' src='"+e.image+"' />"+
												"<em class='fa fa-play-circle-o' program_id='"+e.timestamp+"'"+
												" title='"+e.showName+": EP "+e.episodenumber+":"+e.episodetitle+"'></em>"+
												"</a>"+
												"</div>"+
												"</div>"+
												"<div class='col-xs-12 col-sm-8'>"+
												//episode info
												"<p class='tv-rating'>("+conv_secs(e.duration)+")</p>"+
												"<p class='episode-desc'>"+e.synopsis+"</p>"+
												"<ul class='list-inline'>"+
													"<li><a data-toggle='tooltip' href='#' title='"+e.share_content+"'><em class='fa fa-share-alt'></em></a></li>"+
													"<li><a data-toggle='tooltip' href='http://www.iontelevision.com/show/"+e.slug+"' title='View "+e.showName+"'><em class='fa fa-share'></em></a></li>"+
												"</ul>"+
												"</div></div></div></div>"
						);
					});
				}
			 });
			 
			load_post_schedule(date);
			
		}).fail(function(data) {
			alert( "error:"+ data);
		});
}

function load_post_schedule(date)
{
		CUR_SEL_DATE = date;
		$('.panel-group.schedule .schedule-item').click(function(e){
		$('.drop-icon').removeClass('fa-angle-up').addClass('fa-angle-down');
		if(!$(this).find('.panel-collapse').hasClass('in') ){
			$(this).find('.drop-icon').removeClass('fa-angle-down').addClass('fa-angle-up');
		} else $(this).find('.drop-icon').removeClass('fa-angle-up').addClass('fa-angle-down');
		var selectedItem = $(this).find('.panel-heading').attr('id');
		smooth_scroll(selectedItem);
		});
			 var selectedDate = moment(date).format('MM-DD');
			 var hour = moment().format('HH');
			 selectedDate = selectedDate+"-"+hour;
			 var curDate = moment().format('MM-DD');
			 curDate = curDate+"-"+hour;
			 var cur_hour_show_id ="";
			 if(curDate == selectedDate){
				 cur_hour_show_id = $("[data-isotime='"+curDate+"']").attr('id');
				$("#"+cur_hour_show_id).find('.episode-desc').html("<h2>ON NOW</h2>");
			 }
			 else{
				 var cc = moment(CUR_SEL_DATE).format('YYYY,MM,DD');
				var futureDate = moment(cc).fromNow('dd');
				 cur_hour_show_id = $("[data-isotime='"+selectedDate+"']").attr('id');
				 $("#"+cur_hour_show_id).find('.episode-desc').html("<h2>ON IN "+futureDate+"</h2>");
			 }
			$("#"+cur_hour_show_id).collapse('show');
			smooth_scroll(cur_hour_show_id); //scroll to cur time on load
			 
			$('[data-toggle="tooltip"]').tooltip({placement: 'auto bottom'});
}

function post_top_schedule()
{
	$(".change-date").click(function(e){
		var cl = ['darker-bg','selected-date'];
		var thisdate = $(this).data('date');
		var today = $(this).parent().parent().children().removeClass(cl[0]).find('h3').removeClass(cl[1]);
		$('.change-date').removeClass(cl[0]);
		$('.change-date').removeClass(cl[1]);
		$(this).addClass(cl[0]);
		$(this).find('h3').addClass(cl[1]);
		 load_schedule(thisdate);
	});
}

function show_top_schedule(date)
{
	var start = moment(date).format('d'); //cheated and uses moment.js 
	var startDate = moment(CUR_SEL_DATE).format('MMM D');
	var days = ['SUN','MON','TUE','WED','THU','FRI','SAT'];
	var out = "<div class='col-xs-8 col-sm-2 darker-bg change-date' data-date='"+date+"'>"+
	"<h3 class='selected-date'>TODAY<br><small>"+startDate+"</small></h3></div>"+
	"<div class='col-sm-8 col-xs-12'>";
		days.splice(start,1);
	 var offsetDays = days.slice(start);  
	 var daysAdd = days.splice(0,start);
	 offsetDays = offsetDays.concat(daysAdd);
	$.each(offsetDays,function(i,val){
		var newDate = moment(date).day(i+2).format('MMM D');
		var newDateLink = moment(date).day(i+2).format('YYYY-MM-DD');
		out = out + "<div class='col-sm-2 change-date' data-date='"+newDateLink+"'><h3>"+
		val+"<br><small>"+newDate+"</small></h3></div>";
	});
	out = out + "</div>";
	$("#top_schedule").find('.days_select').html(out);
	post_top_schedule();
}
function show_times(time,time_zone)
{
	//var airdate = moment(time).format('MMM Do');
	var hours = moment(time).format('h');
	 var ampm = moment(time).format('A');
	var centraltime = hours-1;
	if(centraltime == 0 ) centraltime = 12;
return hours+"|"+centraltime+"<span class='meridian'>c "+ampm+"</span>";
}
function conv_secs(secs)
{
	var mins = parseInt(secs/60);
	var minStr = "MINS";
	if(mins <= 1) minStr = "MIN";
	return mins+" "+minStr;
}

function smooth_scroll(id){
    var elem = $("a[name='"+ id +"']");
    if ( typeof( elem.offset() ) === "undefined" ) {
      elem = $("#"+id);
    }
    if ( typeof( elem.offset() ) !== "undefined" ) {
      $('html, body').animate({
              scrollTop: elem.offset().top-260
      }, 1000 );
    }
  };

 var CUR_SEL_DATE; 
 var START_DATE;
 
$(document).ready(function(){

	var dnow = new Date();
	var cleanDate = moment(dnow);
	START_DATE = cleanDate;
	$("#cur_time").html(moment(cleanDate).format("dddd, MMM Do - h:mma"));
	cleanDate = moment(cleanDate).format('YYYY-MM-DD');
	if(moment(dnow).isSameOrBefore(cleanDate+'T00:00:00-05:00')===false)
	{ 
		load_schedule(cleanDate);
		show_top_schedule(cleanDate);
	}
	else 
	{
		var daybefore = moment(dnow).day("0");
		daybefore = moment(daybefore).format('YYYY-MM-DD');
		load_schedule(daybefore);
		show_top_schedule(daybefore);
	}

	$('.next-week').click(function(){
		$('.prev-week').removeClass('disabled');
		var next_week = moment(CUR_SEL_DATE).add(1,'w');CUR_SEL_DATE = next_week;
		show_top_schedule(next_week);
	});
	$('.prev-week').click(function(){
		if(moment(CUR_SEL_DATE).isSameOrBefore(START_DATE)){
		$('.prev-week').addClass('disabled');}
		else{
		var prev_week = moment(CUR_SEL_DATE).subtract(1,'w');CUR_SEL_DATE = prev_week;
		show_top_schedule(prev_week);}
	});
});