<?php // header("Location: http://dev-cmichaels.iontelevision.com/digital-banners/DB_970x90/");
$dirs = array_filter(glob('*'), 'is_dir');

function get_size($in)
{
	$params = explode("_",$in);
	$size = explode("x",$params[1]);
	return array("width" => $size[0], "height" => $size[1]);
}
 ?>
 
 <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ION Television | Digital Banners</title>

   <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body{background-color:#333; }
    	#canvas{background-color:#ddd; width:300px; height:250px; margin:0 auto; text-align:center;}
    	iframe{display:block; margin:0 auto; text-align:center;  border:none;}
    </style>
    
  </head>
  <body>
<div class="container" style="margin-top:1%">
  	<div class="row">
  		<div class="col-md-6 col-md-offset-3">
  			<div class="panel panel-default">
  				<div class="panel-heading"><h3>Digital Banners</h3><small>Click size to load | Double click to open in new window</small></div>
  				<div class="panel-body">
  				<ul class="list-group">
    			<?php foreach($dirs as $dir){ ?>
    
    				<li class="list-group-item ad"><span data-folder="<?php echo $dir; ?>"><?php echo $dir; ?></span></li>
    
    			<?php }?>
    			</ul>
    		
    			</div>
    			</div><!-- end panel-->
    			</div>
    		</div> 
    		<div class="row text-center" >
		<div class="text-center params"><i class="fa fa-refresh fa-spin"></i>  Loading</div>
			<div id="canvas">
				<iframe id="iframe" src="DB2_300x250/index.html" width="300" height="250" scrolling="no" seamless="seamless" align="middle"></iframe>
		</div>
			</div>
    </div> --><!-- end container -->
    


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  <script>
  
      $(document).ready(function(){
      		var canvas = $("#canvas");
      		var iframe = $("#iframe");
      		
      		$('.ad span').dblclick(function(){
      			var url = $(this).data('folder');
      			window.location.href = url;
      		});
      		
      		$('.ad span').click(function(){
      				var url = $(this).data('folder');
      				var params = url.split("_");
      				$('.params').text(params[1]);
      				var size = params[1].split("x");
      				$(iframe).css({
      						width: size[0],
      						height: size[1]
      				});
      				$(iframe).attr("src",url+"/index.html");
      				$(canvas).css({
      						width: size[0],
      						height: size[1]
      						});
      				
      		});
      		
      		$('.ad span').hover(function(){
      		        
      			
      				
      				
      				
      		});
      
      });
  </script>
  </body>
</html>