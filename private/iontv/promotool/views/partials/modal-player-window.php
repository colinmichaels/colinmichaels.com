<div id="modal" class="modal grey darken-3">
    <div class="modal-content">
        <div class="row">
            <div class="col s9"><?php include('inc/brightcove_player.php'); ?></div>
            <div class="col s3">
                <div class="row">
                    <div class="col s12">
                        <h4 class="center-align white-text">Video Title</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <a class="modal-action modal-close btn waves-effect waves-light green btn-large" style="width:100%;">APPROVE <i class="material-icons left">check</i></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <a class="modal-action modal-close btn waves-effect waves-light red btn-large" style="width:100%;">REJECT <i class="material-icons left">close</i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="modal-footer grey darken-1">
        <div class="container-fluid">
            <div class="col s12">
                <div class="container">
                    <ul id="player_actions" class='tabs hide-on-small-and-down row grey darken-1 '>
                        <li class='tab'><a class='waves-effect waves-light btn-flat' href='#'><i
                                    class="material-icons left">edit</i>Edit</a></li>
                        <li class='tab'><a class='waves-effect waves-light btn-flat' href='#'><i
                                    class="material-icons left ">link</i>Click to copy URL</a></li>
                        <li class='tab'><a class='waves-effect waves-light btn-flat' href='#'><i
                                    class="material-icons left">file_download</i>Download</a></li>
                        <li class='tab'><a class='waves-effect waves-light btn-flat' href='#'><i
                                    class="material-icons left">link</i>External Link</a></li>

                    </ul>
                </div>
            </div>
        </div>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat white-text"><i
                class="material-icons">highlight_off</i> Close</a>
    </div>
</div>