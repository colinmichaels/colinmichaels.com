<div id="modal-bottom" class="modal  grey darken-3">
    <div class="modal-content">
        <div class="row">
            <div class="col s4">
                <?php include('inc/make_playlist.php'); ?>
            </div>
            <div class="col s8">
                <?php include('inc/brightcove_player.php'); ?>
            </div>
        </div>
    </div>
    <div class="modal-footer grey darken-1">
        <div class="container-fluid">
            <div class="col s12">
                <div class="container">
                    <ul id="player_actions" class='tabs hide-on-small-and-down row grey darken-1 '>
                        <li class='tab'><a class='waves-effect waves-light btn-flat' href='#'><i
                                    class="material-icons left">edit</i>Edit</a></li>
                        <li class='tab'><a class='waves-effect waves-light btn-flat' href='#'><i
                                    class="material-icons left ">link</i>Click to copy URL</a></li>
                        <li class='tab'><a class='waves-effect waves-light btn-flat' href='#'><i
                                    class="material-icons left">file_download</i>Download</a></li>
                        <li class='tab'><a class='waves-effect waves-light btn-flat' href='#'><i
                                    class="material-icons left">link</i>External Link</a></li>

                    </ul>
                </div>
            </div>
        </div>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat white-text"><i
                class="material-icons">highlight_off</i> Close</a>
    </div>
</div>