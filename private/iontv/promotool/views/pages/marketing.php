<div id="marketing" class="col s12">
    <ul class="collapsible popout" data-collapsible="accordion">
        <li>
            <div class="collapsible-header active grey darken-2 white-text"><i class="material-icons">movie</i>SIZZLES
            </div>
            <div class="collapsible-body">
                <?php include('inc/make_table.php'); ?>
            </div>
        </li>
        <li>
            <div class="collapsible-header grey darken-2 white-text"><i class="material-icons">label</i>CLIENT SOLUTIONS
            </div>
            <div class="collapsible-body"><?php include('inc/make_table.php'); ?></div>
        </li>
        <li>
            <div class="collapsible-header grey darken-2 white-text"><i class="material-icons">label</i>CAMPAIGN RECAPS</div>
            <div class="collapsible-body"></div>
        </li>
        <li>
            <div class="collapsible-header grey darken-2 white-text"><i class="material-icons">label</i>PAID MEDIA</div>
            <div class="collapsible-body"></div>
        </li>
        <li>
            <div class="collapsible-header grey darken-2 white-text"><i class="material-icons">label</i>PSAS</div>
            <div class="collapsible-body"></div>
        </li>
    </ul>
</div>