<?php $base = basename(__FILE__, '.php'); ?>
<div id="<?= $base ?>" class="col s12">
    <ul class="collapsible popout" data-collapsible="accordion">
        <li>
            <div class="collapsible-header active grey darken-2 white-text"><i class="material-icons">timelapse</i>PENDING <?= strtoupper($base) ?>
            </div>
            <div class="collapsible-body">
                <?php include('inc/make_table.php'); ?>
            </div>
        </li>
        <li>
            <div class="collapsible-header grey darken-2 white-text"><i class="material-icons">check</i>APPROVED <?= strtoupper($base) ?>
            </div>
            <div class="collapsible-body"><?php include('inc/make_table.php'); ?></div>
        </li>
        <li>
            <div class="collapsible-header grey darken-2 white-text"><i class="material-icons">block</i>REJECTED <?= strtoupper($base) ?></div>
            <div class="collapsible-body"><?php include('inc/make_table.php'); ?></div>
        </li>
    </ul>
</div>