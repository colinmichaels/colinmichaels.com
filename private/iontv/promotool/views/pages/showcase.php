<?php $base = basename(__FILE__, '.php');

$sections = array(
  "episodic",
    "movies",
    "generic image",
    "end credit",
    "menu",
    "psas",
    "sizzles",
    "client solutions",
    "dr",
    "ion life",
    "qubo",
    "misc"
);


?>
<div id="<?= $base ?>" class="col s12">
    <ul class="collapsible popout" data-collapsible="accordion">

        <?php for($i=0; $i<count($sections); $i++):?>
        <li>
            <div class="collapsible-header active grey darken-2 white-text"><i class="material-icons">label</i> <?= strtoupper($sections[$i]) ?>
            </div>
            <div class="collapsible-body">
                <?php endfor; ?>
            </div>
        </li>

    </ul>
</div>