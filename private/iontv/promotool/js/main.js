$('.dropdown-button').dropdown({
    constrain_width: false, // Does not change width of dropdown to that of the activator
    hover: true, // Activate on hover
    belowOrigin: true // Displays dropdown below the button

});

$('.dropdown-content li a').on('click', function () {

    var clicked = $(this).after().text();

    Materialize.toast("clicked: " + clicked, 3000);
});
$('select').material_select();
$('.carousel').carousel();
$(".button-collapse").sideNav();
$('ul.tabs').tabs();

$('.watch, .playlist').leanModal({
    dismissible: true

});

$('.tablesorter').DataTable({
    "paging": false,
    "searching": false,
    "stateSave": true,
    "autoWidth" : false,
    "info": false
});


$('.dropdown-options').contextmenu(function(event){

    event.preventDefault();
    // Show contextmenu
    $(".dropdown-options-items").finish().toggle(100).
    // In the right position (the mouse)
    css({
        top: event.pageY + "px",
        left: event.pageX + "px"
    });
});

$('li.action').mouseenter(function(){
    console.log(this);
   $(this).children('ul.subnav').stop().slideDown();
}).mouseleave(function(){
    $(this).children('ul.subnav').stop().slideUp();
});

//$(document).contextmenu(function(event){
//   event.preventDefault();
//});
$(document).bind("mousedown", function (e) {

    // If the clicked element is not the menu
    if (!$(e.target).parents(".dropdown-options-items").length > 0) {

        // Hide it
        $(".dropdown-options-items").hide(100);
    }
});