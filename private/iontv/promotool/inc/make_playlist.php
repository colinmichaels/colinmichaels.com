<table class="hover centered bordered tablesorter">
    <thead>
    <tr class="grey">
        <th>#</th>
        <th>NAME</th>
        <th>WATCH</th>
        <th>APPROVE/REJECT</th>
    </tr>
    </thead>

    <tbody>

    <?php for ($i = 0; $i < mt_rand(3,8); $i++) { ?>
        <tr class="grey lighten-1">
            <td><?php echo($i + 1); ?></td>
            <td><?php echo strtoupper($faker->bs); ?> </td>
            <td class="grey lighten-2"><a href="#" class="waves-effect waves-light btn light-blue tooltipped"
                                          data-tooltip="WATCH"><i class="material-icons">visibility</i></a></td>
            <td>
                <div class="collection">
                <a class="collection-item white-text waves-effect waves-light green">APPROVE <i
                        class="material-icons left">check</i></a>
                <a class="collection-item  white-text waves-effect waves-light red">REJECT
                    <i class="material-icons left">close</i></a>
                </div>
            </td>
        </tr>
    <?php } ?>

    </tbody>
</table>