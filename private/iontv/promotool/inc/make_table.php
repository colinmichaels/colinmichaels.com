<table class="hover centered striped bordered tablesorter">
    <thead>
    <tr class="grey">
        <th>PROMO TYPE</th>
        <th>CATEGORY</th>
        <th>ISCI CODE</th>
        <th class="light-blue darken-4 white-text">REVIEW</th>
        <th class="light-blue darken-4 white-text">STATUS</th>

    </tr>
    </thead>
    <tfoot class="hide-on-small-only">
    <tr class="grey">
        <td></td>
        <td></td>
        <td class="center-align">PLAY ALL</td>
        <td colspan="2" class="grey lighten-2"><a href="#modal-bottom"
                                             class="waves-effect  waves-light btn grey darken-3 playlist"
                                             title="PLAYLIST"><i class="material-icons">playlist_play</i></a>
        </td>


    </tr>
    </tfoot>
    <tbody>
    <?php for ($i = 0; $i < mt_rand(3, 12); $i++) { ?>
        <tr class="grey lighten-1">
            <td><?php echo strtoupper($faker->company); ?> </td>
            <td><?php echo strtoupper($faker->company); ?> </td>
            <td><a href="#" class="dropdown-options" ><?php echo $faker->ean8; ?></a></td>
            <td class="grey lighten-2"><?php if ($faker->boolean(75)) { ?><a href="#modal"
                                                                             class="btn btn-sm light-blue waves-effect waves-light watch"
                                                                             title="WATCH"><i
                        class="material-icons">play_circle_outline</i></a><?php } else echo "..."; ?></td>
            <td class="grey lighten-2">

                <?php for ($i = 0; $i < count($reviewers); $i++) {

                    if ($faker->boolean(65)) {

                        echo "<div class='chip white right' id='user-" . $i . "'>";

                        if ($faker->boolean(50)) {
                            echo "<img class='yellow z-depth-1' src='img/approve.svg'/> ";
                            echo strtoupper($reviewers[$i]) . "</div>";
                        } else {
                            echo "<img class='red z-depth-1' src='img/reject.svg'/> ";
                            echo strtoupper($reviewers[$i]) . "</div>";
                            break;
                        }
                    }

                } ?>

            </td>
            <!--<td class="grey lighten-2 hide-on-small-only"><?php /*if ($faker->boolean(50)) { */ ?><i
                    class="material-icons small <?php /*if ($faker->boolean(75)) { */ ?>green-text <?php /*} else echo "red-text"; */ ?>">
                        check_circle</i><?php /*} else echo "..."; */ ?></td>-->
        </tr>
    <?php } ?>

    </tbody>
</table>