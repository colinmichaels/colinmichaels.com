<!-- Start of Brightcove Player -->


<!--
By use of this code snippet, I agree to the Brightcove Publisher T and C
found at https://accounts.brightcove.com/en/terms-and-conditions/.
-->

<object id="myExperience3799094439001" class="BrightcoveExperience responsive-img">
    <param name="bgcolor" value="#FFFFFF"/>

    <param name="height" value="540"/>
    <param name="playerID" value="4744915291001"/>
    <param name="playerKey" value="AQ~~,AAADVn3yV-k~,W1_h_SzaZ0MP7HaiUhEIvwU4_GTEG8yZ"/>
    <param name="isVid" value="true"/>
    <param name="isUI" value="true"/>
    <param name="dynamicStreaming" value="true"/>
    <param name="@videoPlayer" value="3799094439001"/>
</object>

<!--
This script tag will cause the Brightcove Players defined above it to be created as soon
as the line is read by the browser. If you wish to have the player instantiated only after
the rest of the HTML is processed and the page load is complete, remove the line.
-->

<!-- End of Brightcove Player -->