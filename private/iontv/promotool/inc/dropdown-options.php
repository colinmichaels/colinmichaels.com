<div class='dropdown-options-items' class="dropdown-content">
<ul>
    <li class="action"><span class="move_promo"><i class="material-icons right">swap_horizontal</i>Move to</span>
        <ul class="subnav small display_subnav">
            <li class="action"><span class="move_promo">On Air</span>
                <ul class="subnav">
                    <li><span class="move_promo_to_group" group="1" category="49"
                                               group_name="On-Air" category_name="Airbox">Airbox</span></li>
                    <li><span class="move_promo_to_group" group="1" category="5"
                                               group_name="On-Air" category_name="Episodic">episodic</span></li>
                    <li><span class="move_promo_to_group" group="1" category="2"
                                               group_name="On-Air" category_name="Movies">movies</span></li>
                    <li><span class="move_promo_to_group" group="1" category="3"
                                               group_name="On-Air" category_name="Generic Image">generic image</span>
                    </li>
                    <li><span class="move_promo_to_group" group="1" category="16"
                                               group_name="On-Air" category_name="End Credit">end credit</span></li>
                    <li><span class="move_promo_to_group" group="1" category="17"
                                               group_name="On-Air" category_name="Menu">menu</span></li>
                    <li><span class="move_promo_to_group" group="1" category="18"
                                               group_name="On-Air" category_name="PSAs">PSAs</span></li>
                    <li><span class="move_promo_to_group" group="1" category="45"
                                               group_name="On-Air" category_name="Sizzles">sizzles</span></li>
                    <li><span class="move_promo_to_group" group="1" category="43"
                                               group_name="On-Air"
                                               category_name="Client Solutions">client solutions</span></li>
                    <li><span class="move_promo_to_group" group="1" category="44"
                                               group_name="On-Air" category_name="Paid Media">Paid Media</span></li>
                </ul>
            </li>
            <li><span class="move_promo move_promo_to_group" group="4" category="48"
                                       group_name="Airbox">Airbox (ON-AIR)</span></li>
            <li class="action"><span class="move_promo">Showcase</span>
                <ul class="subnav">
                    <li><span class="move_promo_to_group" group="5" category="30"
                                               group_name="Showcase" category_name="Episodic">episodic</span></li>
                    <li><span class="move_promo_to_group" group="5" category="31"
                                               group_name="Showcase" category_name="Movies">movies</span></li>
                    <li><span class="move_promo_to_group" group="5" category="32"
                                               group_name="Showcase" category_name="Generic Image">generic image</span>
                    </li>
                    <li><span class="move_promo_to_group" group="5" category="33"
                                               group_name="Showcase" category_name="End Credit">end credit</span></li>
                    <li><span class="move_promo_to_group" group="5" category="34"
                                               group_name="Showcase" category_name="Menu">menu</span></li>
                    <li><span class="move_promo_to_group" group="5" category="35"
                                               group_name="Showcase" category_name="PSAs">PSAs</span></li>
                    <li><span class="move_promo_to_group" group="5" category="36"
                                               group_name="Showcase" category_name="Sizzles">sizzles</span></li>
                    <li><span class="move_promo_to_group" group="5" category="37"
                                               group_name="Showcase"
                                               category_name="Client Solutions">client solutions</span></li>
                    <li><span class="move_promo_to_group" group="5" category="39"
                                               group_name="Showcase" category_name="DR">DR</span></li>
                    <li><span class="move_promo_to_group" group="5" category="40"
                                               group_name="Showcase" category_name="ION Life">ION Life</span></li>
                    <li><span class="move_promo_to_group" group="5" category="41"
                                               group_name="Showcase" category_name="Qubo">Qubo</span></li>
                    <li><span class="move_promo_to_group" group="5" category="42"
                                               group_name="Showcase" category_name="Misc">Misc</span></li>
                </ul>
            </li>
            <li class="action"><span class="move_promo">Marketing</span>
                <ul class="subnav">
                    <li><span class="move_promo_to_group" group="3" category="22"
                                               group_name="Marketing" category_name="Sizzles">sizzles</span></li>
                    <li><span class="move_promo_to_group" group="3" category="23"
                                               group_name="Marketing"
                                               category_name="Client Solutions">client solutions</span></li>
                    <li><span class="move_promo_to_group" group="3" category="24"
                                               group_name="Marketing"
                                               category_name="Campaign Recaps">Campaign Recaps</span></li>
                    <li><span class="move_promo_to_group" group="3" category="25"
                                               group_name="Marketing" category_name="Paid Media">Paid Media</span></li>
                    <li><span class="move_promo_to_group" group="3" category="29"
                                               group_name="Marketing" category_name="PSAs">PSAs</span></li>
                </ul>
            </li>
            <li class="action"><span class="move_promo">Airbox</span>
                <ul class="subnav">
                    <li><span class="move_promo_to_group" group="4" category="20"
                                               group_name="Airbox" category_name="Sizzles">Sizzles</span></li>
                    <li><span class="move_promo_to_group" group="4" category="26"
                                               group_name="Airbox" category_name="DR">DR</span></li>
                    <li><span class="move_promo_to_group" group="4" category="27"
                                               group_name="Airbox" category_name="Instructionals">Instructionals</span>
                    </li>
                    <li><span class="move_promo_to_group" group="4" category="28"
                                               group_name="Airbox" category_name="Promos">Promos</span></li>
                    <li><span class="move_promo_to_group" group="4" category="47"
                                               group_name="Airbox" category_name="Misc">Misc</span></li>
                    <li><span class="move_promo_to_group" group="4" category="48"
                                               group_name="Airbox" category_name="Airbox">Airbox</span></li>
                </ul>
            </li>
        </ul>
    </li>
    <li class="action"><span class="copy_promo"><i class="material-icons right">content_copy</i>Copy to</span>
        <ul class="subnav small">
            <li><span class="copy_promo">On-Air</span>
                <ul class="subnav">
                    <li><span class="copy_promo_to_group" group="1" category="49"
                                               group_name="On-Air" category_name="Airbox">Airbox</span></li>
                    <li><span class="copy_promo_to_group" group="1" category="5"
                                               group_name="On-Air" category_name="Episodic">episodic</span></li>
                    <li><span class="copy_promo_to_group" group="1" category="2"
                                               group_name="On-Air" category_name="Movies">movies</span></li>
                    <li><span class="copy_promo_to_group" group="1" category="3"
                                               group_name="On-Air" category_name="Generic Image">generic image</span>
                    </li>
                    <li><span class="copy_promo_to_group" group="1" category="16"
                                               group_name="On-Air" category_name="End Credit">end credit</span></li>
                    <li><span class="copy_promo_to_group" group="1" category="17"
                                               group_name="On-Air" category_name="Menu">menu</span></li>
                    <li><span class="copy_promo_to_group" group="1" category="18"
                                               group_name="On-Air" category_name="PSAs">PSAs</span></li>
                    <li><span class="copy_promo_to_group" group="1" category="45"
                                               group_name="On-Air" category_name="Sizzles">sizzles</span></li>
                    <li><span class="copy_promo_to_group" group="1" category="43"
                                               group_name="On-Air"
                                               category_name="Client Solutions">client solutions</span></li>
                    <li><span class="copy_promo_to_group" group="1" category="44"
                                               group_name="On-Air" category_name="Paid Media">Paid Media</span></li>
                </ul>
            </li>
            <li><span class="copy_promo copy_promo_to_group" group="4" category="48"
                                       group_name="Airbox">Airbox (ON-AIR)</span></li>
            <li><span class="copy_promo">Showcase</span>
                <ul class="subnav">
                    <li><span class="copy_promo_to_group" group="5" category="30"
                                               group_name="Showcase" category_name="Episodic">episodic</span></li>
                    <li><span class="copy_promo_to_group" group="5" category="31"
                                               group_name="Showcase" category_name="Movies">movies</span></li>
                    <li><span class="copy_promo_to_group" group="5" category="32"
                                               group_name="Showcase" category_name="Generic Image">generic image</span>
                    </li>
                    <li><span class="copy_promo_to_group" group="5" category="33"
                                               group_name="Showcase" category_name="End Credit">end credit</span></li>
                    <li><span class="copy_promo_to_group" group="5" category="34"
                                               group_name="Showcase" category_name="Menu">menu</span></li>
                    <li><span class="copy_promo_to_group" group="5" category="35"
                                               group_name="Showcase" category_name="PSAs">PSAs</span></li>
                    <li><span class="copy_promo_to_group" group="5" category="36"
                                               group_name="Showcase" category_name="Sizzles">sizzles</span></li>
                    <li><span class="copy_promo_to_group" group="5" category="37"
                                               group_name="Showcase"
                                               category_name="Client Solutions">client solutions</span></li>
                    <li><span class="copy_promo_to_group" group="5" category="39"
                                               group_name="Showcase" category_name="DR">DR</span></li>
                    <li><span class="copy_promo_to_group" group="5" category="40"
                                               group_name="Showcase" category_name="ION Life">ION Life</span></li>
                    <li><span class="copy_promo_to_group" group="5" category="41"
                                               group_name="Showcase" category_name="Qubo">Qubo</span></li>
                    <li><span class="copy_promo_to_group" group="5" category="42"
                                               group_name="Showcase" category_name="Misc">Misc</span></li>
                </ul>
            </li>
            <li><span class="copy_promo">Marketing</span>
                <ul class="subnav">
                    <li><span class="copy_promo_to_group" group="3" category="22"
                                               group_name="Marketing" category_name="Sizzles">sizzles</span></li>
                    <li><span class="copy_promo_to_group" group="3" category="23"
                                               group_name="Marketing"
                                               category_name="Client Solutions">client solutions</span></li>
                    <li><span class="copy_promo_to_group" group="3" category="24"
                                               group_name="Marketing"
                                               category_name="Campaign Recaps">Campaign Recaps</span></li>
                    <li><span class="copy_promo_to_group" group="3" category="25"
                                               group_name="Marketing" category_name="Paid Media">Paid Media</span></li>
                    <li><span class="copy_promo_to_group" group="3" category="29"
                                               group_name="Marketing" category_name="PSAs">PSAs</span></li>
                </ul>
            </li>
            <li><span class="copy_promo">Airbox</span>
                <ul class="subnav">
                    <li><span class="copy_promo_to_group" group="4" category="20"
                                               group_name="Airbox" category_name="Sizzles">Sizzles</span></li>
                    <li><span class="copy_promo_to_group" group="4" category="26"
                                               group_name="Airbox" category_name="DR">DR</span></li>
                    <li><span class="copy_promo_to_group" group="4" category="27"
                                               group_name="Airbox" category_name="Instructionals">Instructionals</span>
                    </li>
                    <li><span class="copy_promo_to_group" group="4" category="28"
                                               group_name="Airbox" category_name="Promos">Promos</span></li>
                    <li><span class="copy_promo_to_group" group="4" category="47"
                                               group_name="Airbox" category_name="Misc">Misc</span></li>
                    <li><span class="copy_promo_to_group" group="4" category="48"
                                               group_name="Airbox" category_name="Airbox">Airbox</span></li>
                </ul>
            </li>
        </ul>
    </li>
    <li class="action"><span class="remove"><i class="material-icons right red-text">remove_circle</i>Remove</span>
        <ul class="subnav">
            <li></li>
            <li><span class="remove_promo_from_group"> Remove One </span></li>
        </ul>
    </li>
    <li class="action"><span class="unpublish_promo"><i class="material-icons right">videocam_off</i> Unpublish  </span></li>
    <li class="action"><span class="force_approve"><i class="material-icons right green-text">done_all</i>Force Approve </span>
        <ul class="subnav">

            <li><i class="material-icons left">person</i><span class="approver" approver_id="114">Adams </span></li>
            <li><i class="material-icons left">person</i><span class="approver" approver_id="45">Addeo </span></li>
            <li><i class="material-icons left">person</i><span class="approver" approver_id="248">Burgess </span></li>
        </ul>
    </li>
    <li><span class="clear_status"><i class="material-icons right">clear</i>Clear Statuses  </span></li>
    <li><span class="edit_promo  "><i class="material-icons right">edit</i>Edit</span></li>
    <li class="action"><span class="info"><i class="material-icons right blue-text">info</i>Info</span>
        <ul class="subnav">
            <li>
                <div class="card-panel"> Uploaded by Griffin Partilla<br> on Feb 17, 2015 at 11:44 AM <br></div>
            </li>
        </ul>
    </li>
    <li><span class="export_promo" promo_id="8116"><i class="material-icons right">link</i>External Link</span></li>
</ul>
</div>