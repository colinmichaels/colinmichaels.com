<?php

error_reporting(E_ALL);

$menu_items = array(
    "on air",
    "psa",
    "marketing",
    "airbox",
    "showcase",
    "archive"
);

$table_headings = array(
    "promo type",
    "category",
    "isci code",

);

$reviewers = array(
    "adams",
    "speller",
    "addeo"
);

//add more names for testing
for($i=0; $i<2; $i++)
{
    $reviewers[] .= $faker->lastname;
}
$reviewers[] .= "Final";


//$LOGO = "img/ion-media-networks-logo.jpg";
$LOGO = "img/ion-logo.svg";
