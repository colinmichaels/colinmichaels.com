<!DOCTYPE html>
<html>
  <head>
      <!--
      Programmed By: Colin Michaels
       Colin Michaels.com
        -->

    <title>Colin Michaels |  Programmer, Web Developer</title>
     <meta name="author" content="Colin Michaels"/>
    <meta name="description" content="Colin Michaels is a creative web developer, programmer, and engineer that is well versed in many computer fields."/> 
    <meta name="keywords" content="ColinMichaels, Web Developer, Web designer, Skilled Computer Specialist, West Palm Beach Florida, Java, Mysql, Graphic Design, PHP , Mysql , html, Information Technology, css, game design, programming, programmer, Colin Michaels, jquery, javascript, html5, css3,java "/>
    <meta name="robots" content="ALL"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="content-language" content="en"/>
    <link rel="icon" 
      type="image/ico" 
      href="images/favicon.ico"/>
    
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery.qtip.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.13.custom.css"/>
    <link rel="stylesheet" type="text/css" href="css/orangebox.css"/>
    
    <!--<script type="text/javascript" src="js/jquery.js"></script>-->


    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/jquery-color.js"></script>
    <script type="text/javascript" src="js/jquery-slideshow.js"></script>
    <script type="text/javascript" src="js/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="js/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
    <script type="text/javascript" src="js/orangebox.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
   <!-- <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>-->
    <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-13296389-1']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
    
    
  </head>
  <body onload="checkCookie();">
      <h3 style="color:#000;">Colin Michaels, Web Developer</h3>
      <div id="page_wrap">
 

      <div id="background"></div>

      <div id="mediaBox">
           
              <a href="https://www.facebook.com/colin.michaels?iframe" rel="lightbox" title="Facebook" target="_blank">
                  <img src="images/logos/fb-logo.png" alt="facebook" height="32px" width="32px"/></a>
               <a href="http://colinmichaels.tumblr.com/"  title="Tumblr" target="_blank">
              <img src="images/logos/tumblr_logo.png" alt="tumblr" height="32px" width="32px"/></a>
              <a href="http://www.linkedin.com/in/colinmichaels?iframe" rel="lightbox" title="Linkedin" target="_blank">
              <img src="images/logos/linkedin-logo.png" alt="linkedin" height="32px" width="32px"/></a>
              <a href="php/twitter.php?iframe" rel="lightbox"  title="Twitter" target="_blank">
              <img src="images/logos/twitter-logo.png" alt="twitter" height="32px" width="32px"/></a>
              <a href="mailto:colinmichaels@gmail.com"  title="Gmail" target="_blank">
              <img src="images/logos/gmail-logo.png" alt="gmail" height="32px" width="32px"/></a>
               
          

         
                
      </div>
       
      <div id="bannerWrap">
          <div id="colintop"></div>
		<div id="bannerBody">
                        <a class="hoverBtn" id="but_slideshow" onclick="tog_menu(this.id);">Home</a>
                        <a class="hoverBtn" id="but_mywork"    onclick="tog_menu(this.id);" title="Examples of my work">Work</a>
                        <!--<a class="hoverBtn" id="but_blog"       href="/blog" target="_blank" title="ColinOfAllTrades">Blog</a>-->
                        <a class="hoverBtn" id="but_photos"     href="https://plus.google.com/photos/106302167830737414150/albums/5676766673758244113" target="_blank"  title="My Photography" >Photos</a>
                        <a class="hoverBtn" id="but_videos"     href="http://www.youtube.com/oddypro2" target="_blank"   title="My Videos">Videos</a>
                        <a class="hoverBtn" id="but_aboutme"   onclick="tog_menu(this.id);" title="About me">About</a>
			<a class="hoverBtn" id="but_links"     onclick="tog_menu(this.id);" title="Some great sites">Links</a>
			<a class="hoverBtn" id="but_contact"   onclick="tog_menu(this.id);">Contact</a>
                 <div class="clear"></div>
                </div>
            
          
       </div>
 <div id="displayItems">
    
      <!-- Slideshow HTML -->
  <div id="slideshow" class="box_bkg">
    <div id="slidesContainer">
        
      <div class="slide">
        <!--<h2><g:plusone size="small"></g:plusone> 
        <iframe src="http://www.facebook.com/plugins/like.php?app_id=181096281948329&amp;href=http%3A%2F%2Fcolinmichaels.com&amp;send=false&amp;layout=button_count&amp;width=150&amp;show_faces=false&amp;action=like&amp;colorscheme=dark&amp;font=tahoma&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:150px; height:21px;" allowTransparency="true"></iframe>
        </h2>-->
        
        <p style="font-size: 1.5em;"><img src="images/slides/slide_programming.png" alt="Colin Michaels Computer programmer. Java, Javascript, HTML, CSS, VB.NET, MySql, PHP"  usemap="#programming_slide_map" width="215" height="145"/>
        </p>
            <div class="prog_examples"> <a title="Click on any of the languages below to see an example"><h3>Programming examples</h3> </a>
        <ul id="prog_examples-links" >
                <li><a href="examples/build.php?doc=php?iframe" rel="lightbox[code]" title="PHP" >PHP</a></li>
                <li><a href="examples/build.php?doc=javascript?iframe" rel="lightbox[code]" title="Javascript">Javascript</a></li>
                <li><a href="examples/build.php?doc=vb?iframe" rel="lightbox[code]" title="Visual Basic" >Visual Basic</a></li>
                <li><a href="examples/build.php?doc=sql?iframe" rel="lightbox[code]" title="MySQL" >MySql</a></li>
                <li><a href="examples/build.php?doc=css?iframe" rel="lightbox[code]" title="CSS">CSS</a></li>
                <li><a href="examples/build.php?doc=html?iframe" rel="lightbox[code]" title="HTML/XHTML">HTML/XHTML</a></li>
                <li><a href="examples/build.php?doc=js?iframe" rel="lightbox[code]" title="JQuery">JQuery</a></li>
                <li><a href="examples/build.php?doc=java?iframe" rel="lightbox[code]" title="Java">Java</a></li>
                <li><a href="examples/build.php?doc=c?iframe" rel="lightbox[code]" title="C/C++">C/C++</a></li>
            </ul>
        </div>
        <map id="programming_slide_map" name="programming_slide_map">
        <a href="examples/build.php?doc=php?iframe"  rel="lightbox[code]" title="PHP Example">       <area shape="rect" coords="10,14,70,49" href="#" alt="PHP" title="PHP"    /></a>
        <a href="examples/build.php?doc=javascript?iframe" rel="lightbox[code]" title="JavaScript Example"><area shape="rect" coords="83,9,210,33" href="#" alt="JAVASCRIPT" title="JAVASCRIPT"    /></a>
        <a href="examples/build.php?doc=vb?iframe" rel="lightbox[code]" title="Visual Basic Example">        <area shape="rect" coords="83,32,210,56" href="#" alt="VB" title="VB"    /></a>
        <a href="examples/build.php?doc=sql?iframe" rel="lightbox[code]" title="MySQL Example">      <area shape="rect" coords="81,56,208,80" href="#" alt="MY-SQL" title="MY-SQL"    /></a>
        <a href="examples/build.php?doc=css?iframe" rel="lightbox[code]" title="CSS Example">      <area shape="rect" coords="83,81,210,105" href="#" alt="CSS" title="CSS"    /></a>
        <a href="examples/build.php?doc=html?iframe" rel="lightbox[code]" title ="HTML Example">      <area shape="rect" coords="83,108,210,132" href="#" alt="HTML" title="HTML"    /></a>
        <a href="examples/build.php?doc=js?iframe" rel="lightbox[code]" title="JQuery Example">      <area shape="rect" coords="0,112,80,136" href="#" alt="JQUERY" title="JQUERY"    /></a>
        <a href="examples/build.php?doc=java?iframe" rel="lightbox[code]" title="Java Example">      <area shape="rect" coords="3,83,83,107" href="#" alt="JAVA" title="JAVA"    /></a>
        <a href="examples/build.php?doc=c?iframe" rel="lightbox[code]" title="C/C++ Example">      <area shape="rect" coords="0,58,80,82" href="#" alt="C/C++" title="C/C++"    /></a>
        </map>
        
        
            <h2 style="float:right;"><a href="http://colinmichaels.com/php/resume.php?iframe" rel="lightbox" title="Colin Michaels' Resume">RESUME</a></h2>
        

        </div>
      <div id="WebDesign" class="slide">
        <h2>Web Design</h2>
        <p><a href="#"><img src="images/slides/slide_webdesign.png" width="215" height="145" alt="Colin Michaels- Web Designs - Photoshop, Dreamweaver, Flash, CS5." title="Web Design Examples"/></a>
        <p><a  href="http://quinncom.net?iframe" rel="lightbox" title="QuinnCom.net" target="_blank">QuinnCom.net</a></p>
        <p><a  href="http://drinksnowater.com/?iframe" rel="lightbox" title="drinksnowater.com" target="_blank">drinksnowater.com</a></p>
        <p><a  href="http://budschicken.com?iframe" rel="lightbox" title="Buds Chicken" target="_blank">Budschicken.com</a></p>
        <p><a  href="http://devilslabyrinth.com?iframe" rel="lightbox" title="Devils Labyrinth" target="_blank">DevilsLabyrinth.com</a></p>
        <p><a  href="http://madpeagames.com/DarkRoom?iframe" rel="lightbox" title="DarkRoom" target="_blank">The Dark Room</a></p>
    
      </div>
      <div id="GameProgramming" class="slide">
        <h2>Game Programming</h2>
        <p><a href="#"><img src="images/slides/slide_gamedesign.png" width="215" height="145" alt="Colin Michaels- Game Designer - The Devil's Labyrinth, The Virtual Medical Doctor, Dark Room Hunt, Chromasphere"/></a>
            MMORPG's ,Virtual world games, .</p>
        <p>More info about the games <a href="http://Devilslabyrinth.com?iframe" title="http://devilslabrinth.com" rel="lightbox">The Devils Labyrinth</a> .. More games. <a href="http://secondlife.com/destination/virtual-medical-doctor?iframe" rel="lightbox[videos]">Virtual Medical Doctor</a> .. more games.</p>
        <p><a href="http://www.youtube.com/watch?v=HanBGORzJVA" rel="lightbox" title="Devil's Labyrinth Trailer">DL Trailer</a></p>
        <p><a href="http://www.youtube.com/watch?v=KyUbQ3RFxiA" rel="lightbox" title="Virtual Medical Doctor Trailer">VMD Trailer</a></p>
      </div>
      <div id="Music" class="slide">
        <h2>Music/Sound Design</h2>
        <p><a href="http://allmusic.com/artist/colin-michaels-p609632/credits"><img src="images/slides/slide_credits.png" width="215" height="145" alt="Colin Michaels- Audio Engineer - Sound Design, Mixing , Tracking, Editing." /></a></p>
        <p>Audio Engineering and Sound Design is a passion of mine. I spent 10 years in the music business where I got to work with some of the worlds biggest artists. Through my work I was awarded with 3 Latin Grammy Awards for my mixing work on <a href="http://lacalle13.com/?iframe" rel="lightbox" title="Calle 13" target="_blank">'Calle 13'</a> debut album. &nbsp;<a href="http://allmusic.com/artist/colin-michaels-p609632/credits?iframe" rel="lightbox" target="_blank">Credit List</a>.</p>
      </div>
        
        
    </div>
      
      
  </div>

          <div id="mywork" class="hidden">
              <h1 style="display:none;">Colin Michaels, Web Developer</h1>
              
            
              <h1> My work </h1>
              <?php include("php/mywork.php")?>
          </div>
          <!-- END portfolio -->
           <!-- About Me info slide over -->
          <div id="aboutme" class="hidden">

              <h1> About me </h1>
              <?php include("php/aboutme.php")?>
          </div>
          <!-- END About Me -->
           <!-- Links slide over -->
          <div id="links" class="hidden">
                   <h1> Links </h1>
                    <?php include("php/links.php");?>
          </div>
          <!-- END links -->
         
           <!-- Contact info slide over -->
          <div id="contact" class="hidden">

              <h1> Contact </h1>
              <?php include("php/email.php");?>
          </div>
          <!-- END Contact Info -->
           
 
 </div><!--End display items div -->
 
  <div id="footer">
      <br />
          <a id="bar_slideshow" onclick="tog_menu(this.id);">HOME</a> |
          <a id="bar_mywork"    onclick="tog_menu(this.id);">My Work</a> |
          <a id="bar_aboutme"   onclick="tog_menu(this.id);">About me</a> |
          <a id="bar_links"     onclick="tog_menu(this.id);">Links</a> |
          <a id="bar_contact"   onclick="tog_menu(this.id);">Contact</a>
          <br />
          <span style="font-size: 0.6em;">Powered By Colin's Brain.</span>
  </div>



 </div>  <!-- End page wrap -->
 <script id="aptureScript" type="text/javascript">
(function (){var a=document.createElement("script");a.defer="true";a.src="http://www.apture.com/js/apture.js?siteToken=nd77nxQ";document.getElementsByTagName("head")[0].appendChild(a);})();
</script>
  </body>
</html>