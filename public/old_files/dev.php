<?php
// test email link referer from notification email
extract($_REQUEST);
$action = $_REQUEST['link'];
if($action=='gmail_graduation_notification')
{
$refer = $_SERVER['HTTP_REFERER'];
$host = $_SERVER['REMOTE_ADDR'];
$headers = 'From: colin@colinmichels.com' . "\r\n" .
    'Reply-To: colin@colinmichaels.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$mail = mail("colinmichaels@gmail.com",$action,$refer.$host."\n....Tracking Notifier......\n",$headers);
}


?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN'
    'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
  <head>
      <!--
      Programmed By: Colin Michaels
       Colin Michaels.com
        -->

    <title>Colin Michaels  Programmer, Web Designer</title>
    <meta name="description" content="Colin Michaels is a creative designer, programmer, and engineer that is well versed in many computer fields."/>
    <meta name="keywords" content="IT, Florida, Java, Mysql, Graphic Design, PHP , Mysql , html, Information Technology, css, game design, programming, programmer, Colin , Michaels, jquery, javascript, html5, css3,java "/>
    <meta name="robots" content="ALL"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="content-language" content="en"/>
    <link rel="icon" 
      type="image/ico" 
      href="images/favicon.ico">
    
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery.qtip.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.13.custom.css"/>
    <link rel="stylesheet" type="text/css" href="css/orangebox.css"/>
    
    <!--<script type="text/javascript" src="js/jquery.js"></script>-->


    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/jquery-color.js"></script>
    <script type="text/javascript" src="js/jquery-slideshow.js"></script>
    <script type="text/javascript" src="js/jquery.qtip.min.js"></script>
    <script type="text/javascript" src="js/jquery.jscrollpane.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
    <script type="text/javascript" src="js/orangebox.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
   <!-- <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>-->
    <script type="text/javascript">

      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-13296389-1']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
    
    
  </head>
  <body onload="checkCookie();">
      
      <div id="page_wrap">
      <div id="displaybox" class="hidden">
          <!--<img src="/images/menubar/colinmichaels.png" alt="ColinMichaels"/>-->
          <p>
          <br />Welcome to ColinMichaels.com This site is to show examples of my work and offer information about me.  
          </p>
          <div id="closebox" onclick="displayBox(0);" >Click to Close</div>
          <form id="signature" action="">
              <p style="font-size: 0.5em">sign the guest book below with your name</p>
              
              <p style="font-size: 0.7em">Name:<input type="text" name="vistor" size="10" onclick="displayBox(2);"/></p>
          </form>
          
      </div>


      <div id="background"></div>

      <div id="mediaBox">
           
              <a href="https://www.facebook.com/colin.michaels?iframe" rel="lightbox" title="Facebook" target="_blank">
                  <img src="images/logos/fb-logo.png" alt="facebook" height="32px" width="32px"/></a>
              <a href="http://www.linkedin.com/in/colinmichaels?iframe" rel="lightbox" title="Linkedin" target="_blank">
              <img src="images/logos/linkedin-logo.png" alt="linkedin" height="32px" width="32px"/></a>
              <a href="php/twitter.php?iframe" rel="lightbox"  title="Twitter" target="_blank">
              <img src="images/logos/twitter-logo.png" alt="twitter" height="32px" width="32px"/></a>
              <a href="mailto:colinmichaels@gmail.com"  title="Gmail" target="_blank">
              <img src="images/logos/gmail-logo.png" alt="gmail" height="32px" width="32px"/></a>
          

         
                
      </div>
       
      <div id="bannerWrap">
          <div id="colintop"></div>
		<div id="bannerBody">
                        <a class="hoverBtn" id="but_slideshow" onclick="tog_menu(this.id);">Home</a>
                        <a class="hoverBtn" id="but_mywork"    onclick="tog_menu(this.id);">My Work</a>
			<a class="hoverBtn" id="but_aboutme"   onclick="tog_menu(this.id);">About Me</a>
			<a class="hoverBtn" id="but_links"     onclick="tog_menu(this.id);">Links</a>
			<a class="hoverBtn" id="but_contact"   onclick="tog_menu(this.id);">Contact</a>
                 <div class="clear"></div>
                </div>
            
          
       </div>
        <div id="news_blog_L" class="side_panels"><?php include('php/news.php');?></div>
        <div id="news_blog_R" class="side_panels"><?php include('php/blog.php');?></div>
 <div id="displayItems">
    
      <!-- Slideshow HTML -->
  <div id="slideshow" class="box_bkg">
    <div id="slidesContainer">
        
      <div class="slide">
        <!--<h2><g:plusone size="small"></g:plusone> 
        <iframe src="http://www.facebook.com/plugins/like.php?app_id=181096281948329&amp;href=http%3A%2F%2Fcolinmichaels.com&amp;send=false&amp;layout=button_count&amp;width=150&amp;show_faces=false&amp;action=like&amp;colorscheme=dark&amp;font=tahoma&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:150px; height:21px;" allowTransparency="true"></iframe>
        </h2>-->
       
        <p style="font-size: 1.5em;"><a href="#/"><img src="images/slides/slide_programming.png" alt="Colin Michaels Computer programmer. Java, Javascript, HTML, CSS, VB.NET, MySql, PHP"  usemap="#programming_slide_map" width="215" height="145"/></a>
        </p>
            <div class="prog_examples"> <a title="Click on any of the languages below to see an example"><h3>Programming examples</h3> </a>
        <ul id="prog_examples-links" >
                <li><a href="examples/build.php?doc=php?iframe" rel="lightbox[code]" title="PHP" >PHP</a></li>
                <li><a href="examples/build.php?doc=javascript?iframe" rel="lightbox[code]" title="Javascript">Javascript</a></li>
                <li><a href="examples/build.php?doc=vb?iframe" rel="lightbox[code]" title="Visual Basic" >Visual Basic</a></li>
                <li><a href="examples/build.php?doc=sql?iframe" rel="lightbox[code]" title="MySQL" >MySql</a></li>
                <li><a href="examples/build.php?doc=css?iframe" rel="lightbox[code]" title="CSS">CSS</a></li>
                <li><a href="examples/build.php?doc=html?iframe" rel="lightbox[code]" title="HTML/XHTML">HTML/XHTML</a></li>
                <li><a href="examples/build.php?doc=js?iframe" rel="lightbox[code]" title="JQuery">JQuery</a></li>
                <li><a href="examples/build.php?doc=java?iframe" rel="lightbox[code]"title="Java">Java</a></li>
                <li><a href="examples/build.php?doc=c?iframe" rel="lightbox[code]" title="C/C++">C/C++</a></li>
            </ul>
        </div>
        <map id="programming_slide_map" name="#programming_slide_map">
        <a href="examples/build.php?doc=php?iframe"  rel="lightbox[code]" title="PHP Example">       <area shape="rect" coords="10,14,70,49" href="#" alt="PHP" title="PHP"    /></a>
        <a href="examples/build.php?doc=javascript?iframe" rel="lightbox[code]" title="JavaScript Example"><area shape="rect" coords="83,9,210,33" href="#" alt="JAVASCRIPT" title="JAVASCRIPT"    /></a>
        <a href="examples/build.php?doc=vb?iframe" rel="lightbox[code]" title="Visual Basic Example">        <area shape="rect" coords="83,32,210,56" href="#" alt="VB" title="VB"    /></a>
        <a href="examples/build.php?doc=sql?iframe" rel="lightbox[code]" title="MySQL Example">      <area shape="rect" coords="81,56,208,80" href="#" alt="MY-SQL" title="MY-SQL"    /></a>
        <a href="examples/build.php?doc=css?iframe" rel="lightbox[code]" title="CSS Example">      <area shape="rect" coords="83,81,210,105" href="#" alt="CSS" title="CSS"    /></a>
        <a href="examples/build.php?doc=html?iframe" rel="lightbox[code]" title ="HTML Example">      <area shape="rect" coords="83,108,210,132" href="#" alt="HTML" title="HTML"    /></a>
        <a href="examples/build.php?doc=js?iframe" rel="lightbox[code]" title="JQuery Example">      <area shape="rect" coords="0,112,80,136" href="#" alt="JQUERY" title="JQUERY"    /></a>
        <a href="examples/build.php?doc=java?iframe" rel="lightbox[code]"title="Java Example">      <area shape="rect" coords="3,83,83,107" href="#" alt="JAVA" title="JAVA"    /></a>
        <a href="examples/build.php?doc=c?iframe" rel="lightbox[code]" title="C/C++ Example">      <area shape="rect" coords="0,58,80,82" href="#" alt="C/C++" title="C/C++"    /></a>
        </map>
        
        
            
        

        </div>
      <div class="slide">
        <h2>Web Design</h2>
        <p><a href="#"><img src="images/slides/slide_webdesign.png" width="215" height="145" alt="Colin Michaels- Web Designs - Photoshop, Dreamweaver, Flash, CS5." title="Web Design Examples"/></a>
        <a  href="http://devilslabyrinth.com?iframe" rel="lightbox" title="Devils Labyrinth" target="_blank">DevilsLabyrinth.com</a></p>
        <p><a  href="http://madpeagames.com?iframe" rel="lightbox" title="MadPeaGames" target="_blank">MadPeaGames</a></p>
        <p><a  href="http://madpeagames.com/DarkRoom?iframe" rel="lightbox" title="DarkRoom" target="_blank">The Dark Room</a></p>
        <p><a  href="http://waterwayguide.com?iframe" rel="lightbox" title="WaterWayGuide.com" target="_blank">WaterWay Guide</a></p>
        <p><a  href="http://colinmichaels.com/flash?iframe" rel="lightbox" title="ColinMichaels.com (Flash Version)" target="_blank">ColinMichaels.com (Flash Version)</a></p>
    
      </div>
      <div class="slide">
        <h2>Game Programming</h2>
        <p><a href="#"><img src="images/slides/slide_gamedesign.png" width="215" height="145" alt="Colin Michaels- Game Designer - The Devil's Labyrinth, The Virtual Medical Doctor, Dark Room Hunt, Chromasphere"/></a>
            MMORPG's ,Virtual world games, .</p>
        <p>More info about the games <a href="http://Devilslabyrinth.com?iframe" title="http://devilslabrinth.com" rel="lightbox"">The Devils Labyrinth</a> .. More games. <a href="http://secondlife.com/destination/virtual-medical-doctor?iframe" rel="lightbox[videos]">Virtual Medical Doctor</a> .. more games.</p>
        <p><a href="http://www.youtube.com/watch?v=HanBGORzJVA" rel="lightbox" title="Devil's Labyrinth Trailer">DL Trailer</a></p>
        <p><a href="http://www.youtube.com/watch?v=KyUbQ3RFxiA" rel="lightbox" title="Virtual Medical Doctor Trailer">VMD Trailer</a></p>
      </div>
      <div class="slide">
        <h2>Music/Sound Design</h2>
        <p><a href="#"><img src="images/slides/slide_credits.png" width="215" height="145" alt="Colin Michaels- Audio Engineer - Sound Design, Mixing , Tracking, Editing." /></a></p>
        <p>Audio Engineering and Sound Design is a passion of mine. I spent 10 years in the music business where I got to work with some of the worlds biggest artists. Through my work I was awarded with 3 Latin Grammy Awards for my mixing work on <a href="http://lacalle13.com/?iframe" rel="lightbox" title="Calle 13" target="_blank">'Calle 13'</a> debut album. &nbsp;<a href="http://allmusic.com/artist/colin-michaels-p609632/credits?iframe" rel="lightbox" target="_blank">Credit List</a>.</p>
      </div>
        
        
    </div>
      
      
  </div>

          <div id="mywork" class="hidden">
            
              <h1> My work </h1>
              <?php include("php/mywork.php")?>
          </div>
          <!-- END portfolio -->
           <!-- About Me info slide over -->
          <div id="aboutme" class="hidden">

              <h1> About me </h1>
              <?php include("php/aboutme.php")?>
          </div>
          <!-- END About Me -->
           <!-- Links slide over -->
          <div id="links" class="hidden">
                   <h1> Links </h1>
                    <?php include("php/links.php");?>
          </div>
          <!-- END links -->
         
           <!-- Contact info slide over -->
          <div id="contact" class="hidden">

              <h1> Contact </h1>
              <?php include("php/email.php");?>
          </div>
          <!-- END Contact Info -->
           
 
 </div><!--End display items div -->
 
  <div id="footer">
      <p> <a id="bar_slideshow" onclick="tog_menu(this.id);">HOME</a> |
          <a id="bar_mywork"    onclick="tog_menu(this.id);">My Work</a> |
          <a id="bar_aboutme"   onclick="tog_menu(this.id);">About me</a> |
          <a id="bar_links"     onclick="tog_menu(this.id);">Links</a> |
          <a id="bar_contact"   onclick="tog_menu(this.id);">Contact</a>
      <div id="powered">Powered By Colin's Brain.<br />
      
          <br />  <br /> 
        <!-- <script type="text/javascript"><!--
        google_ad_client = "ca-pub-9178551814780918";
        /* Test_ads */
        google_ad_slot = "9509545354";
        google_ad_width = 468;
        google_ad_height = 15;
        //-->
      <!--  </script>
        <script type="text/javascript"
        src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
        </script>-->
        
      </div>
       
       <div id="ww3tags" class="hidden">
           <a href="http://validator.w3.org/check?uri=referer?iframe" rel="lightbox"><img
                src="images/logos/valid-html401.png"
                alt="Valid HTML 4.01 Transitional" height="20" width="65"/></a>
            <a href="http://jigsaw.w3.org/css-validator/check/referer?iframe" rel="lightbox">
            <img style="border:0;width:65px;height:20px"
                src="images/logos/vcss-bw.png"
                alt="Valid CSS!" /></a>

        </div>
  </div>

      </div>
 <script id="aptureScript" type="text/javascript">
(function (){var a=document.createElement("script");a.defer="true";a.src="http://www.apture.com/js/apture.js?siteToken=nd77nxQ";document.getElementsByTagName("head")[0].appendChild(a);})();
</script>
  </body>
</html>