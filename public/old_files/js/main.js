
 var hoverColour = "#FFF";
    var x;
    
    $(document).ready(function() {
    


      $('a[title],input[title]').each(function()
            {
               
                $(this).qtip({
                    content:{  
                        
                        ajax: {
                            url: 'php/hoverinfo.php',
                            data: {id:$(this).attr('title')},
                            once: false,
                            success: function(data,status){
                                            this.set('content.text',data)
                            }
                        }
                    
                
                    },

                   style: {
                                classes: 'ui-tooltip-dark ui-tooltip-rounded'
                           },

                   position: { adjust:{x:-15, y:10},
                                  my:'top-right',
                                  at: 'top-right',
                                  //target: 'mouse',
                                  effect: function(api, offset) {
                                  $(this).animate(offset, 10, 'swing'); // "this" refers to the tooltip
                               }
                   }
                });
            });

                $("a.hoverBtn").show("fast", function() {
                        $(this).wrap("<div class=\"hoverBtn\">");
                        $(this).attr("class", "");

                });

                //display the hover div
                $("div.hoverBtn").show("fast", function() {
                        //append the background div
                        $(this).append("<div></div>");

                        //get link's size
                        var wid = $(this).children("a").width();
                        var hei = $(this).children("a").height();

                        //set div's size
                        $(this).width(wid);
                        $(this).height(hei);
                        $(this).children("div").width(wid);
                        $(this).children("div").height(hei);

                        //on link hover
                        $(this).children("a").hover(function(){
                                //store initial link colour
                                if ($(this).attr("rel") == "") {
                                        $(this).attr("rel", $(this).css("color"));
                                }
                                //fade in the background
                                $(this).parent().children("div")
                                        .stop()
                                        .css({"display": "none", "opacity": "1"})
                                        .fadeIn("fast");
                                //fade the colour
                                $(this)	.stop()
                                        .css({"color": $(this).attr("rel")})
                                        .animate({"color": hoverColour}, 350);
                        },function(){
                                //fade out the background
                                $(this).parent().children("div")
                                        .stop()
                                        .fadeOut("slow");
                                //fade the colour
                                $(this)	.stop()
                                        .animate({"color": $(this).attr("rel")}, 250);
                        });
                });
                
                $("#contact_form").validate({
                        rules: {
				name:   {required:  true},
				email:  {required:  true},
                                subject: {required: true}
                                  
			},
			messages: {
				name:   "Please let me know who you are.",
				email:  "Please enter your email address.",
                                subject: "Please enter a subject."
                                
                        },
			submitHandler: function(form){
                            $("#results").dialog({
                                            height:400, modal:true,
                                            buttons: {
                                            Submit: function() {
                                                    $.post('php/send_email.php', $("#contact_form").serialize(), function(data) {
                                                        $('#results').html(data).dialog("close");
                                                     })
                                            },
                                            Cancel: function() {
                                                    $( this ).dialog( "close" );
                                             }
                                            }
                                            });
                         
                        }
        
                    });
                $("button,input:submit,input:reset").button();
                
                
                if(navigator.appName.indexOf('Internet Explorer')<=0)// IE workaround
                {
                    $("#page_wrap").jScrollPane({showArrows:true, animateEase:"slow",verticalDragMaxHeight:30, horizontalDragMaxWidth:30});
                }
        });
                

function tog_menu(id)
{
    var totDivs = document.getElementById("displayItems").getElementsByTagName("div").length;
    //alert(totDivs.toString());
    for( var i = 0; i< totDivs; i++)
        {
            //if(document.getElementById("displayItems").getElementsByTagName("div")[i].id == id)
              //  {
                 $(document.getElementById("displayItems").getElementsByTagName("div")[i]).hide().addClass("hidden");
              //  }
            
        }
    //substring will take a start and finish param .. on 'substr(start,end) leave out the end to take till the end'
    var idChk = id.substr(4);
    if(idChk=="slideshow")
        {
            var getalldivs = document.getElementById(idChk).getElementsByTagName("div").length;
            for( i = 0; i< totDivs; i++)
                {

                         $(document.getElementById(idChk).getElementsByTagName("div")[i]).show().removeClass("hidden");
                      

                }
        }
    var parseId = "#"+idChk;
    //alert(parseId.toString());
    //$("#slideshow").fadeOut("slow");
    $(parseId).fadeIn("slow").removeClass("hidden");
}



function displayBox(tog)
{
    if(tog==0)
        {
            $("#displaybox").stop().fadeOut("slow");
            
         }
   else if (tog==1)
        {
            $("#displaybox").fadeIn(1000).delay(15000).fadeOut(2000);
        }
   else if(tog==2)
            {
               
              $("#displaybox").fadeIn();
            }
  
}

function checkCookie()
        {
            var c_title = "ColinMichaels_opening";
            var cname=getCookie(c_title);
                
            if (cname!=null && cname!="")
            {
                // cname = cookie name to be stored
                // alert("Yummy Cookie");
            }
            else
            {
               // alert("no Cookie");
                if (cname!=null && cname!="")
                {
                     displayBox(1);
                    setCookie(c_title,"visited",3);
                }
            }
        }
function setCookie(c_name,value,exdays)
        {
            var exdate=new Date();
            exdate.setDate(exdate.getDate() + exdays);
            var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
            document.cookie=c_name + "=" + c_value;
        }
function getCookie(c_name)
        {
            var i,x,y,ARRcookies=document.cookie.split(";");
            for (i=0;i<ARRcookies.length;i++)
            {
                x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
                y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
                x=x.replace(/^\s+|\s+$/g,"");
                if (x==c_name)
                {
                    return unescape(y);
                }
                else return 0;
            }
        }
function tog_drop(menu_item,tog)
{
    var item = "#"+menu_item+"-links";
    if(tog==1)
        {
                
                $(item).stop().slideDown("slow");
                $(item).removeClass("hidden");
                //$(item).fadeIn("slow");
        }
    else if(tog==0)
        {
            //var all = document.ElementsByTagName("ul").length;
            $(item).slideUp("slow");
            //$(item).addClass("hidden");
        }
}

function getTitle()
{
    
    var a=document.getElementsByTagName('a');
                for(var i=0; i<a.length; i++)
                {
                    a[i].anum=i;
                    a[i].onmouseover=function()
                    {
                        
                        var z = this.anum+1;
                        return z;
                       /* var y = $("a").eq(z);
                        var x= y.attr("title");
                        if(x!=="")
                            { return(z);}
                            else
                                {return NaN;}
                                */
                    }
                }
}

