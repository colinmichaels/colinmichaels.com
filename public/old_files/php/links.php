<ul class="links">
    <h2>Programming Links</h2>
    <li><a href="http://code.google.com/apis/ajax/playground/" rel="lightbox" title="Google Code Playground">Google Code Playground</a></li>
    <li><a href="http://www.jquery.com?iframe" rel="lightbox" title="Jquery">Jquery</a></li>
    <li><a href="http://www.cplusplus.com?iframe" rel="lightbox" title="C++ Programming Tutorials">C++ Programming Tutorials</a></li>
    <li><a href="http://www.php.net?iframe" rel="lightbox" title="PHP.net">PHP programming reference site</a></li>
    <li><a href="http://cs.fit.edu/~ryan/java/language/basics.html?iframe" rel="lightbox" title="Basics of the Java language">Basics of Java</a></li>
    <li><a href="http://msdn.microsoft.com/en-us/beginner/bb308732.aspx?iframe" rel="lightbox" title="Web and Programming development Basics">Windows Development Basics</a></li>
    <li><a href="http://www.w3schools.com?iframe" rel="lightbox" title="w3schools">Web Development Tutorials</a></li>
    
    <h2>Interesting Sites and Tidbits</h2>

<li><a href="http://www.partha.com/?iframe" rel="lightbox" title="GIMP & Related Musings">Pathra</a></li>
    <li><a href="http://www.altdevblogaday.com//?iframe" rel="lightbox" title="Game Developers Blog">#AltDevBlog</a></li>
    <li><a href="http://www.chromeexperiments.com/?iframe" rel="lightbox" title="Html5 Chrome Experiements">Chrome Experiments</a></li>
    <li><a href="http://www.dreamspark.com/?iframe" rel="lightbox" title="Dream Spark">Microsoft Dreamspark</a></li>
    <li><a href="http://www.psdgraphics.com//?iframe" rel="lightbox" title="PSD Template Graphics">PSD Template Graphics</a></li>
    <li><a href="http://www.fontsquirrel.com/fontface/generator/?iframe" rel="lightbox" title="Create your a web font face">Font Squirell</a></li>
    <li><a href="http://www.mrdoob.com?iframe" rel="lightbox" title="Awesome Programmer">Mr Doob!</a></li>
    
    <h2>Networking Links</h2>

    <li><a href="http://network-tools.com//?iframe" rel="lightbox" title="Traceroute, Ping, Domain Name Server (DNS) Lookup, WHOIS">Network-Tools.com</a></li>
    <li><a href="http://www.subnettingquestions.com/?iframe" rel="lightbox" title="Subnetting Quizer">Subnetting Questions</a></li>
    <li><a href="http://cisco.netacad.net/?iframe" rel="lightbox" title="Cisco Network Academy">Cisco Networking Academy</a></li>
    <li><a href="http://www.cisco.com/web/learning/netacad/course_catalog/PacketTracer.html?iframe" rel="lightbox" title="Cisco Packet Tracer">Cisco Packet Tracer</a></li>
    <li><a href="http://www.wireshark.org/?iframe" rel="lightbox" title="Wireshark Protocol Analyzer">Wireshark</a></li>
    
</ul>

 