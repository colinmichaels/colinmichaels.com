<html>
<head>
<link rel="stylesheet" type="text/css" href="http://colinmichaels.com/css/styles.css"/>
</head>
<body>

            <div class="prog_examples"> <a title="Click on any of the languages below to see an example"><h3>Programming Examples</h3> </a>
        <ul id="prog_examples-links" >
                <li><a href="http://colinmichaels.com/examples/build.php?doc=php"  title="PHP" >PHP</a></li>
                <li><a href="http://colinmichaels.com/examples/build.php?doc=javascript"  title="Javascript">Javascript</a></li>
                <li><a href="http://colinmichaels.com/examples/build.php?doc=vb"  title="Visual Basic" >Visual Basic</a></li>
                <li><a href="http://colinmichaels.com/examples/build.php?doc=sql"  title="MySQL" >MySql</a></li>
                <li><a href="http://colinmichaels.com/examples/build.php?doc=css"  title="CSS">CSS</a></li>
                <li><a href="http://colinmichaels.com/examples/build.php?doc=html"  title="HTML/XHTML">HTML/XHTML</a></li>
                <li><a href="http://colinmichaels.com/examples/build.php?doc=js"  title="JQuery">JQuery</a></li>
                <li><a href="http://colinmichaels.com/examples/build.php?doc=java"  title="Java">Java</a></li>
                <li><a href="http://colinmichaels.com/examples/build.php?doc=c"  title="C/C++">C/C++</a></li>
            </ul>
        </div>
        </body>
        </html>
     