
<hr class="hr_spacer"/><h2 style="font-size:1.6em;"><a href="https://docs.google.com/document/d/1SPyAWI0K6riUW7YhsUO-Or18xt8vmzUx9fQCIRAwosY/pub?iframe" rel="lightbox" title="Click
to View my resume">Click for my Resume</a></h2><hr class="hr_spacer"/>
<p> I have a broad background in
everything from print media, web programming, web design, and my current
focus, which is software development. Over the last 15 years I have worked in many
different creative capacities. They include the music and entertainment business,
collateral and print, audio/video mixing and web development. Through these
experiences I have gained the skills which have helped me create successful and profitable
ventures for my clients and past employers. Please take a quick look at my web site for a
small sample of my talents.</p>