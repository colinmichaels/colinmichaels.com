<!doctype html>

<html lang="en">
<head>
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta charset="utf-8">

  <title>HAPPY HALLOWEEN</title>
  <meta name="description" content="Colin Michaels">
  
  <meta http-equiv="Content-Language" content="en">

	<!--Forces latest IE rendering engine & chrome frame-->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<!-- favicon & other link Tags -->
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />  
	<link rel="apple-touch-icon" href="/images/custom_icon.png"/><!-- 114x114 icon for iphones and ipads -->
	<link rel="copyright" href="#copyright" /> 

	<link rel="pingback" href="http://colinmichaels.com/halloween" />
	<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600,800' rel='stylesheet' type='text/css'>-->
	<meta property="og:title" content="Colin Michaels | Halloween Shooter Game"/>
<meta property="og:image" content="http://colinmichaels.com/halloween/images/screenshot.jpg"/>
<meta property="og:site_name" content="Colin Michaels | Halloween Shooter Game"/>
<meta property="og:description" content="Colin Michaels | Halloween Shooter Game , Zombie Shooter"/>

  <meta name="author" content="Colin Michaels">
<link rel="stylesheet" href="css/reset.min.css" />
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Creepster' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="css/styles.css?v=1.0">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>   
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/ui-darkness/jquery-ui.min.css" />
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
<script src="js/ion.sound.min.js"></script>
<script src="js/main.js"></script>
<script src="js/game.js"></script>
<!--<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("jquery", "2.1");
</script>-->

  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body>