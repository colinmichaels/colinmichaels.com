;<!DOCTYPE html>
<html><title>animated random colors</title>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Animated Css/Jquery Colors"/>
<meta property="og:url" content="http://colinmichaels.com/color/"/>
<meta property="og:image" content="http://colinmichaels.com/color/img/colors.jpg"/>
<meta property="og:description"
      content="Random Colors! using Jquery, CSS , and PHP. I needed to think about something else other then what I was working on at the moment so I made this."/>
<head>
    <style>#main {
            -webkit-filter: blur(10px);
            width: 100%;
            height: 100%;
            padding: 1%
        }

        .color {
            min-width: 0.5%;
            min-height: 0.5%;
            float: left;
            padding: .5%
        }</style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>$(document).ready(function () {
            function n() {
                var n = ["red", "blue", "yellow", "black", "green", "BlueViolet", "Fuchsia", "#7CFC00"], o = n.length;
                $(".color").each(function () {
                    rand = Math.floor(Math.random() * o + 1), $(this).css("background-color", n[rand - 1])
                })
            }
            function t(object,blocks,speed){
                build_blocks(object,blocks);
                n(), $(".color").click(function () {
                    n()
                }), setInterval(function () {
                    n()
                }, speed);
            }

            t($("#main"),Math.floor(Math.random() * 5000 + 1),100);

            $("#main").click(function(){
                var rand = Math.floor(Math.random() * 10 + 1);
                t(rand*100);
                $(this).css({
                    'filter'         : 'blur('+rand+'px)',
                    '-webkit-filter' : 'blur('+rand+'px)',
                    '-moz-filter'    : 'blur('+rand+'px)',
                    '-o-filter'      : 'blur('+rand+'px)',
                    '-ms-filter'     : 'blur('+rand+'px)'
                });
            });

            function build_blocks(object,num){
                for(var i = 0; i<= num; i++ ){
                    object.append("<div class='color'></div>");
                }
            }


        });</script>
</head>
<body>
<div id="main" class="color"></div>
</body>
</html>