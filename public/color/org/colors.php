<!DOCTYPE html>
<html>
<title>animated random colors</title>
<meta property="og:type" content="website" />
<meta property="og:title" content="Animated Css/Jquery Colors" />
<meta property="og:url" content="http://colinmichaels.com/color/" />
<meta property="og:image" content="http://colinmichaels.com/color/img/colors.jpg" />
<meta property="og:description" content="Random Colors! using Jquery, CSS , and PHP.   I needed to think about something else other then what I was working on at the moment so I made this." />
<head>
<style>
#main{width:100%; height:100%; padding:1%;}
.color{ min-width:1%; min-height:1%; float:left; padding:0.5%; }
</style>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
$(document).ready(function() {
function rand_color()
{
	var colors = ["red", "blue", "yellow", "black", "green"];
	var cnt = colors.length;
	$('.color').each(function(){
		rand = Math.floor((Math.random() * cnt) + 1);
		$(this).css("background-color" , colors[rand-1]);
});
}
rand_color();
$('.color').click(function(){
	rand_color();
});
setInterval(function(){
	rand_color();
},100 );
});
</script>
</head>
<body>
<div id="main" class="color">
<?php 
$cnt = 5000; 
for($i =0; $i <=$cnt; $i++){?>
<div class='color'></div>
<?php } ?>
</div>
</body>
</html>