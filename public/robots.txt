Sitemap: https://colinmichaels.com/sitemap.xml
User-Agent: *
Disallow:
Disallow: /old_files
Disallow: /audio
Disallow: /iontv
Disallow: /cam
Disallow: /examples
