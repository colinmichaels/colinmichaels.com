<?php

namespace App\Http\Controllers;

use App\Clients\Project;
use App\Post;
use App\Page;
use App\Resume;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactForm;
use Spatie\SchemaOrg\Graph;
use Spatie\SchemaOrg\Schema;
use Spatie\SchemaOrg\LocalBusiness;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
    	$projects = Project::active();
    	$posts = Post::recent();
    	$page = Page::all()->first();
    	$resume = (new Resume)->build();

	    $schema = $this->getSchema();

        return view('home')->with(compact('projects','posts', 'page','resume', 'schema'));
    }


    public function getSchema(){

        echo (new Graph())->organization()
                            ->name('Colin Michaels')
                            ->email('colin@colinmichaels.com')
                            ->address('Jupiter, FL 33458')
                            ->telephone( '909-265-4699');

    	echo (new Schema)->person()
		    ->name('Colin Michaels')
		    ->email('colin@colinmichaels.com')
		    ->address('Jupiter, FL 33458')
		    ->familyName( 'Michaels')
		    ->gender('male')
		    ->givenName( 'Colin')
		    ->jobTitle( 'Digital Director')
		    ->memberOf( 'Delta Upsilon')
		    ->telephone( '909-265-4699')
		    ->toScript();

    }

	/**
	 * @param Request $request
	 *
	 * @return RedirectResponse
	 */
	public function store(Request $request)
	{

	   try {

		   Mail::to( env( 'ADMIN_EMAIL' ) )->send(new ContactForm( $request));

	   }catch(Exception $exception){
	   	dump('ERROR');
	   	dd($exception);
	   }

		if(Mail::failures() < 0){
			$message =  "Thanks for the inquiry.  I will get back to you as soon as humanly possible.  <i class='fa fa-grin-alt'></i>";
		}
		else{
			$message = "There was an issue with your email, please try again. ";
		}
		return redirect(route('home').'#contact')->with(compact('message'));
	}
}
