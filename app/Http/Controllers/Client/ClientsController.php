<?php

namespace App\Http\Controllers;

use App\Mail\NewClientEmail;
use Illuminate\Http\Request;
use App\Client;
use App\Project;
use App\User;
use App\Website;
use App\MediaBuy;
use App\Invoice;
use App\InvoiceStatus;
use DB;
use Illuminate\Http\Response;
use Validator;
use Auth;
use Mail;
use Toastr;

use Maatwebsite\Excel\Excel;

use App\Exports\ClientsExport;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    	$items = Client::all();
	    $page = [
		    'title' => 'Clients',
		    'breadcrumbs' => 'clients',
		    'content' =>     view('ogk.clients.index')->with(compact('items'))
	    ];

	    return view('ogk.default')->with($page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
	    return view('ogk.clients.create')->with($this->getSelectors());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {

	    $client = new Client;
	   $new_client =  DB::transaction(function () use ($request, $client) {
		    $input = $request->all();

		    Validator::make($input, [
			    'media' => 'mimes:jpg,jpeg,gif,png,svg,svg+xml,pdf,doc,docx,mp4',
			    'name' => 'required | unique:clients',
			    'client_code' => 'min:3',
			    'email' => 'required'
		    ])->validate();

		    if(isset($request->media)){

			    $client->addMedia($request->media)->toMediaCollection('media');
		    }

		    return $client->create( $input );


	    });

	    if(isset($request->websites)){

		    $websites = $request->websites;
		    foreach($websites as $website){

			    $website = Website::whereId($website)->first();
			    if($website) {
				    $website->client_id = $client->id;
				    $website->save();
			    }
			    else {
				    $site             = new Website;
				    $site->production = $website;
				    $site->client_id  = $client->id;
				    $site->save();
			    }

		    }

	    }

	    if($new_client && $request->email_client && isset($request->client_email)) {

		    Mail::to( $request->client_email )
		        ->cc(env('ADMIN_EMAIL') )
		        ->send( new NewClientEmail( $new_client ) );
	    }

	    Toastr::success('Client: <strong>'.$new_client->name.'</strong> has been created!', 'New Client Created');


	    return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

	    $client = Client::findOrFail($id);

	    $statuses = InvoiceStatus::all()->pluck('name','id')->toArray();
		$invoices_stats = Invoice::ClientInvoicesStats($client->id);
		$invoices = $client->invoices;

	    return view('ogk.invoices.show')->with(compact('invoices','invoices_stats','statuses'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
	    $client = Client::findOrFail($id);

	    return view('ogk.clients.edit')->with(compact('client'))->with($this->getSelectors());
    }

    public function getSelectors(){
	    $websites = Website::all()->pluck('production','id')->toArray();
	    $clients = User::role('client')->pluck('name','id')->toArray();
	    $users = User::all()->pluck('name','id')->toArray();
	    $projects = Project::all()->pluck('name','id')->toArray();
	    $media_buys = MediaBuy::all()->pluck('name','id')->toArray();
	    $invoices = Invoice::all()->pluck('id')->toArray();

	    return compact('clients','projects','media_buys','websites','users','invoices');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
	    $client = Client::findOrFail($id);
	    $input = $request->all();

	    Validator::make($request->all(), [
		    'media' => 'mimes:jpg,jpeg,gif,png,svg,svg+xml,pdf,doc,docx,mp4'
	    ])->validate();

	    if(isset($request->media)){

		    $client->addMedia($request->media)
		           ->toMediaCollection('media');
	    }
	    if(isset($request->logo)){

		    $client->addMedia($request->logo)
		           ->toMediaCollection('logo');
	    }

	    $client->update($input);

	    if(isset($request->websites)){

		    $websites = $request->websites;
		    foreach($websites as $website){

			    $website = Website::whereId($website)->first();
			    if($website) {
				    $website->client_id = $client->id;
				    $website->save();
			    }
			    else {
				    $site             = new Website;
				    $site->production = $website;
				    $site->client_id  = $client->id;
				    $site->save();
			    }

		    }

	    }

	    Toastr::success('Client: <strong>'.$client->name.'</strong> has been updated!', 'Client Updated');
	    return redirect()->back();
    }

    public function export(Excel $excel, ClientsExport $export){

	    return $excel->download($export, 'OGKClients.xlsx');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
