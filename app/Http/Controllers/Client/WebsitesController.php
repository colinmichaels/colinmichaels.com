<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Website;
use App\Client;
use DB;

use Auth;
use Illuminate\Http\Response;
use Validator;
use Toastr;

class WebsitesController extends Controller
{

	public function __construct() {

		view()->share('clients' , Client::toList());
		view()->share('editor',   Auth::user());


	}
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
	    $items = Website::all();

	    return view('ogk.websites.index')->with(compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
	    return view('ogk.websites.create')->with(compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
    	$website = new Website;
	    DB::transaction(function () use ($request, $website) {

		    if(isset($request->media)){

			    $website->addMedia($request->media)->toMediaCollection('media');
		    }
		    $input = $request->all();
		    $website->create( $input );
	    });

	    Toastr::success('<strong>Website:</strong> '.$website->name.' has been created', 'Website Created');

	    return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
       $user = Auth::user();

       if($user->company)
            $items = $user->company->websites;
       else return redirect()->back();

       return view('ogk.websites.show')->with(compact('items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
    	$website = Website::findOrFail($id);
	    return view('ogk.websites.edit')->with(compact('website','clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
	    $website = Website::findOrFail($id);
	    $input = $request->all();

	    Validator::make($request->all(), [
		    'media' => 'mimes:jpg,jpeg,gif,png,svg,pdf,doc,docx,mp4'
	    ])->validate();

	    if(isset($request->media)){

		    $website->addMedia($request->media)->toMediaCollection('media');
	    }
	    $website->update($input);
	    Toastr::success('<strong>Website:</strong> '.$website->name.' has been updated', 'Website Update');
	    return redirect()->back();
    }

    public function import($file){




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
