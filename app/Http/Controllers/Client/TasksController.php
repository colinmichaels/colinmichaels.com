<?php

namespace App\Http\Controllers;
use App\Mail\TaskAssignedEmail;
use App\Task;
use App\User;
use App\Project;
use Auth;
use Illuminate\Http\Response;
use Validator;
use Carbon;
use DB;
use App\ProjectStatus;
use App\Department;
use Toastr;
use Mail;

use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tasks = Task::all();
        return view('ogk.tasks.index')->with(compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
	    return view('ogk.tasks.create')->with($this->getSelectors());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {

	    $task =  DB::transaction(function () use ($request) {
		    $task  = new Task;
		    $input = $request->except('_token');
		    $task->create( $input );
		    return $task;
	    });

	    return $this->index();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
	    $tasks = Task::where('assigned_to',$id)->get();
	    return view('ogk.tasks.show')->with(compact('tasks'));
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showStatus($id, $status)
	{

		$tasks = Task::where('assigned_to',$id)->where('status_id', $status)->get();
		return view('ogk.tasks.show')->with(compact('tasks','status'));
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
	    $task = Task::findOrFail($id);
	    Toastr::info('<strong>Task:</strong> '.$task->name.' is being edited', 'Task Edit');
        return view('ogk.tasks.edit')->with(compact('task'))->with($this->getSelectors());
    }

    public function getSelectors(){

	    $departments = Department::toList();
	    $statuses = ProjectStatus::toList();
	    $projects = Project::toList();
	    $users = User::toList();   // todo:: split this up to show list of assigners and assignees
	    return compact('departments','statuses','projects','users');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
	    $task = Task::findOrFail($id);
	    $input = $request->all();

	    Validator::make($request->all(), [
		    'media' => 'mimes:jpg,jpeg,gif,png,svg,pdf,doc,docx,mp4'
	    ])->validate();


	    if(isset($request->media)){

	    	$task->addMedia($request->media)->toMediaCollection('media');
	    }

	    $input['start_date'] = (isset($request->start_date))? $request->start_date : $task->start_date;
	    $input['end_date'] = (isset($request->end_date))? $request->end_date : $task->end_date;
	    $input['due_date'] = (isset($request->due_date))? $request->due_date : $task->due_date;
	    $task->update($input);

	    Mail::to( $task->assignee->email )
	        ->cc(env('ADMIN_EMAIL') )
	        ->send( new TaskAssignedEmail( $task ) );

	    Toastr::info('<strong>Task: </strong>'.$task->name.' has been updated!', 'Task Update');
	    return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
