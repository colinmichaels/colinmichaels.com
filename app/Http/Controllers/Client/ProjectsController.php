<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clients\Project;
use App\Clients\ProjectStatus;
use App\Clients\ProjectType;
use App\Clients\ProjectUser;
use App\Client;
use App\Clients\Task;
use App\User;
use Illuminate\Http\Response;
use Wgmv\SlackApi\Facades\SlackChannel;

use DB;
use Cache;
use Carbon;

use Auth;
use Validator;
use Toastr;

class ProjectsController extends Controller
{

	public function __construct() {

		$statuses = Cache::remember('ogk.projects.statuses', Carbon::now()->addDays(1), function(){
			return ProjectStatus::toList();
		});
		view()->share('statuses', $statuses);

		$types = Cache::remember('ogk.projects.types', Carbon::now()->addDays(1), function(){
			return ProjectType::toList();
		});

		view()->share('types'   , $types);

		view()->share('projects', Project::toList());

		$clients = Cache::remember('ogk.clients', Carbon::now()->addMinutes(5), function(){
				return Client::toList();
		});
		view()->share('clients' , $clients);

		$users = Cache::remember('ogk.users', Carbon::now()->addMinutes(30), function(){
			return User::toList();
		});

		view()->share('users'   , $users );
		view()->share('tasks'   , Task::toList() );

		$channel_names = Cache::remember('ogk.slack_channels',Carbon::now()->addMinutes(30), function(){
			$channels = SlackChannel::lists(null,true,true,100);
			return array_map(function($channel){
				return "#".$channel->name;
			}, $channels->channels);
		});

		view()->share('channels', $channel_names);

	}

	/**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $projects = Project::all();
        return view('templates.pages.projects.index')->with(compact('projects'));
	  /*  $items = Project::all();
	    return view('ogk.projects.index')->with(compact('items','statuses'));*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

	    return view('ogk.projects.create')->with(compact('statuses','projects','clients','users','types', 'channels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
	    $project = new Project;
	    DB::transaction(function () use ($request, $project) {

		    if(isset($request->media)){

			    $project->addMedia($request->media)->toMediaCollection('media');
		    }
		    $input = $request->all();
		    $project->create( $input );
	    });
	    Toastr::success('<strong>Project:</strong> '.$project->name.' has been created', 'Project Created');

	    return $this->index();

    }

    public function trafficReport($id){

    	$project = Project::findOrFail($id);
	    return view('ogk.projects.traffic-report')->with(compact('project','statuses','users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
	    $user = Auth::user();
	    if($user->hasRole('client')){
		    if($user->company){
			    $items = $user->company->projects;
			    return view('ogk.projects.show')->with(compact('items','statuses'))->with(['status_id' => $id]);
		    }

		    else return redirect()->back();

	    }else{
		    $items = Project::where('status_id',$id)->get();
		    return view('ogk.projects.index')->with(compact('items','statuses'))->with(['status_id' => $id]);
	    }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
	    $project = Project::findOrFail($id);
	    return view('ogk.projects.edit')->with(compact('project','users','statuses','projects','clients','types','channels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
	    $project = Project::findOrFail($id);
	    $input = $request->all();

	    Validator::make($request->all(), [
		    'media' => 'mimes:jpg,jpeg,gif,png,svg,pdf,doc,docx,mp4'
	    ])->validate();

	    if(isset($request->project_members)){
	        foreach($request->project_members as $member){
	        	$project_member = new ProjectUser;
	        	$project_member->project_id = $id;
	        	$project_member->user_id = $member;
	        	$project_member->save();
	        }
	    }
	    if(isset($request->project_tasks)){
		    foreach($request->project_tasks as $task){
			    $project_task = Task::findOrFail($task);
			    $project_task->project_id = $id;
			    $project_task->save();
		    }
	    }


	    if(isset($request->media)){

		    $project->addMedia($request->media)->toMediaCollection('media');
	    }
	    $project->update($input);
	    Toastr::success('<strong>Project:</strong> '.$project->name.' has been updated', 'Project Update');
	    return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
