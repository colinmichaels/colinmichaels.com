<?php

namespace App\Http\Controllers;

use App\Page;
use App\Resume;
use Illuminate\Http\Request;
use Psr\Http\Message\UriInterface;
use Spatie\Sitemap\SitemapGenerator;

class TestsController extends Controller
{


    public function index()
    {

        dd($this->createSitemap());

    }

    private function createSitemap()
    {
        SitemapGenerator::create(env('DOMAIN_REMOTE'))->shouldCrawl(function (
            UriInterface $url
        ) {
            // All pages will be crawled, except the contact page.
            // Links present on the contact page won't be added to the
            // sitemap unless they are present on a crawlable page.
            $ignore = [
                '/old_files',
                '/media'
            ];

            $check = [];
            foreach ($ignore as $path) {
                array_push($check, strpos($url->getPath(), $path) === false);
            }

            return ( ! in_array(false, $check));

        })->writeToFile(public_path('sitemap.xml'));
    }

    private function getOpenGraph()
    {
        $pages = new Page;

        return $pages->all()->first()->openGraph();
    }

    public function showResume()
    {
        return view('templates.sections.resume')->with(['resume' => (new Resume)->build()]);
    }
}
