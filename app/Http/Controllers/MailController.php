<?php

namespace App\Http\Controllers;

use App\Mail\ContactForm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    //

	public function send(Request $request){

		Mail::to(env('ADMIN_EMAIL'))
		    ->send(new ContactForm($request));
	}
}
