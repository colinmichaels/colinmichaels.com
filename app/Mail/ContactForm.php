<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class ContactForm extends Mailable
{
    use Queueable, SerializesModels;

	/**
	 * @var Request
	 */
	private $request;

    /**
     * Create a new message instance.
     * @var Request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	$request = $this->request;
    	return $this->from( $this->request->email, $this->request->name)
		            ->subject( 'Colin Michaels.com | Contact form from:' . $this->request->name)
	                 ->markdown('vendor.mail.markdown.message')->with(compact('request'));
    }
}
