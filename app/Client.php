<?php
/**
 * Created for OGK Creative.
 * Project: colinmichaels.com
 * Developer: Colin Michaels
 * Date: 2019-06-23
 * Time: 21:37
 */

namespace App;

use Laravel\Cashier\Billable;

use App\User;

class Client extends User
{

    use Billable;



}
