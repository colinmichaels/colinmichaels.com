<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Psr\Http\Message\UriInterface;
use Illuminate\Http\Request;
use Spatie\Sitemap\SitemapGenerator;
use App\Mail\AdminEmail;
use Carbon\Carbon;


class GenerateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate the sitemap.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        SitemapGenerator::create(env('DOMAIN_REMOTE'))->shouldCrawl(function (
            UriInterface $url
        ) {
            // All pages will be crawled, except the contact page.
            // Links present on the contact page won't be added to the
            // sitemap unless they are present on a crawlable page.
            $ignore_paths = config('sitemap.ignore_paths');

            $check = [];
            foreach($ignore_paths as $path){
                array_push($check,strpos($url->getPath(),$path) === false);
            }
            return (!in_array(false,$check));

        })->writeToFile(public_path('sitemap.xml'));

        $request = new Request;
        $request->name = __FILE__;
        $request->email = env('ADMIN_EMAIL');
        $request->comment = "Site Map Generated at ".Carbon::now()->format('Y-m-d h:i:s');

        Mail::to( env( 'ADMIN_EMAIL' ) )->send(new AdminEmail( $request));


    }
}
