<?php
/**
 * Project: colinmichaels.com
 * Developer: Colin Michaels
 * Date: 2019-06-16
 * Time: 00:43
 */

namespace App\Traits;


use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

trait ImageTrait
{
    use HasMediaTrait;

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
             ->width(260)
             ->height(260)
             ->sharpen(10);

        $this->addMediaConversion( 'large')
             ->width(1000)
             ->height(700)
             ->sharpen(10)
             ->withResponsiveImages();

        $this->addMediaConversion( 'medium')
             ->width(400)
             ->height(200)
             ->sharpen(10)
             ->withResponsiveImages();

        $this->addMediaConversion( 'screenshot')
             ->width(1000)
             ->height(700)
             ->sharpen(10)
             ->withResponsiveImages();
    }

    public function getPlaceholderAttribute(){

        $placeholder =  "https://via.placeholder.com/300.jpg?text={$this->title}";
        $this->addMediaFromUrl($placeholder)->toMediaCollection('images');
        return $placeholder;
    }

    public function getImageAttribute(){
        return   ($this->getMedia('images')->isNotEmpty())
            ? $this->getMedia('images')[0]->getFullUrl()
            : $this->screenshot;
    }

    public function getScreenshotAttribute(){
        return ($this->getMedia('screenshots')->isNotEmpty())
            ?$this->getMedia('screenshots')[0]->getFullUrl('screenshot')
            : $this->placeholder;
    }

    public function getThumbAttribute(){
        return ($this->getMedia('images')->isNotEmpty())
            ?$this->getMedia('images')[0]->getFullUrl('thumb')
            : $this->placeholder;
    }

}
