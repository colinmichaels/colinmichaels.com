<?php

namespace App;

use App\Traits\ImageTrait;
use ChrisKonnertz\OpenGraph\OpenGraph;
use Illuminate\Database\Eloquent\Model;
use Request;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class Page extends Model implements HasMedia
{

   use ImageTrait;

   public function meta(){

   }

   public function openGraph(){

       return  (new OpenGraph)->title($this->title)
           ->type('page')
           ->image($this->screenshot)
           ->description($this->description)
           ->url($this->slug)->renderTags();
   }

    public static function get( $id = null ) {
        if ( is_null( $id ) ) {
            $id = Request::path();
        }

        return self::select( $id )->first();
    }


   public function parent(){

   	   return $this->belongsTo('App\Page', 'id','parent_id');

   }

   public function posts(){

   	return $this->hasMany('App\Post', 'page_id', 'id');
   }


	public function scopeSelect($query, $param){
		if(is_numeric($param)){
			$query->where("id",$param);
		}else{
			$query->where("slug",$param);
		}
	}


}
