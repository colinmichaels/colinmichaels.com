<?php

namespace App;

use App\Traits\ImageTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use WebDevEtc\BlogEtc\Models\BlogEtcPost;

class Post extends BlogEtcPost implements HasMedia
{

    use ImageTrait;

	protected $table = "blog_etc_posts";

	public function pages(){

		return $this->belongsToMany('App\Page', 'pages', 'id');
	}

	public function scopeRecent($query){

	    return $query->whereIsPublished(1)->limit(8)->get();
    }


    public function excerpt($limit = 200){

	    return html_entity_decode($post->generate_introduction(200));

    }

	public function scopeSelect($query, $param){
		if(is_numeric($param)){
			$query->where("id",$param);
		}else{
			$query->where("slug",$param);
		}
	}
}
