<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Lead extends Model
{
	use LogsActivity;

	protected static $logName = 'lead';
	protected static $logOnlyDirty = true;
	protected static $logAttributes = ["*"];

	public function getDescriptionForEvent(string $eventName): string
	{
		return "Lead: <strong>" .$this->name."</strong>  has been {$eventName}";
	}
}
