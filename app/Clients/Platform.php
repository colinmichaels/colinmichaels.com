<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{

	public function category(){

		return $this->belongsTo('App\PlatformCategory', 'platform_category_id');
	}
}
