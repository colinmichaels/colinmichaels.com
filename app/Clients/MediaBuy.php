<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\Activitylog\Traits\LogsActivity;

class MediaBuy extends Model implements HasMedia
{
	use HasMediaTrait;
	use LogsActivity;

	protected static $logName = 'media-buy';
	protected static $logOnlyDirty = true;
	protected static $logAttributes = ["*"];

	public function getDescriptionForEvent(string $eventName): string
	{
		return "Media Buy: <strong>" .$this->name."</strong>  has been {$eventName}";
	}

	protected $fillable = [
		  'active',
		'name',
		'status_id',
		'platform_id',
		'vendor_id',
		'client_id',
		'project_id',
		'program',
		'product',
		'start_date',
		'end_date',
		'rate',
		'method_id',
		'net_gross',
		'quantity',
		'net_media_cost',
		'commision',
		'additional_cost'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		"created_at",
		"updated_at",
	];

	public function scopeActive($query){
		$query->whereActive(1);
	}

	public function project() {
		return $this->belongsTo( 'App\Project', 'project_id' );
	}

	public function vendor() {
		return $this->belongsTo( 'App\Vendor', 'vendor_id' );
	}

	public function client(){
		return $this->belongsTo('App\Client');
	}

	public function method(){
		return $this->belongsTo('App\Method');
	}

	public function platform(){
		return $this->belongsTo('App\Platform');
	}

	public function getStatusAttribute(){
		return  $this->hasOne('App\ProjectStatus','id','status_id')->first()->name;
	}

}
