<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

    public function department_head(){

    	return $this->belongsTo('App\User','department_head_id');
    }

	public static function toList(){

		return Department::all()->pluck('name','id')->toArray();

	}
}
