<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;

class TicketAssignment extends Model
{
	public function ticketable()
	{
		return $this->morphTo();
	}
}
