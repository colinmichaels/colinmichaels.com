<?php

namespace App\Clients;

use App\Traits\MediableTrait;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use Spatie\Permission\Traits\HasRoles;

use Spatie\Activitylog\Traits\LogsActivity;

use Carbon;

class Task extends Model implements HasMediaConversions {
	use HasMediaTrait;
	use MediableTrait;
	use HasRoles;
	use LogsActivity;
	use Notifiable;

	protected static $logName = 'task';
	protected static $logOnlyDirty = true;
	protected static $logAttributes = ["*"];

	protected $fillable = [
		'name',
		'project_id',
		'assigned_to',
		'assigned_by',
		'priority',
		'description',
		'department_id',
		'status_id',
		'priority_id',
		'start_date',
		'end_date',
		'due_date',
		'est_hours',
		'hours'
	];
	protected $dates = [
		"start_date",
		"end_date",
		"due_date",
		"created_at",
		"updated_at",
	];

	protected $guard_name = 'web';

	// Specify Slack Webhook URL to route notifications to
	public function routeNotificationForSlack() {
		return env('SLACK_WEBHOOK_URL');
	}

	public function getLinkAttribute(){

		return route('tasks.edit', $this->id);
	}

	public function getDescriptionForEvent(string $eventName): string
	{
		return "Task: <strong>" .$this->name."</strong>  has been {$eventName}";
	}

	public static function toList(){

		return Project::all()->pluck('name','id')->toArray();

	}
	/**
	 * Relationship to project
	 */
	public function project() {
		return $this->belongsTo( 'App\Project', 'project_id' );
	}

	public function department() {
		return $this->belongsTo( 'App\Department', 'department_id' );
	}

	public function assigner() {
		return $this->belongsTo( 'App\User', 'assigned_by' );
	}

	public function assignee() {
		return $this->belongsTo( 'App\User', 'assigned_to' );
	}

	public function tickets(){
		return $this->morphMany('App\TicketAssignment', 'ticketable');
	}

	public function status(){
		return  $this->belongsTo('App\ProjectStatus','status_id');
	}


	public static function scopeThisMonth($query){

		$query->whereBetween('created_at', [Carbon::now()->subMonth(), Carbon::now()]);
	}

	public function CreatedBetween($start, $end){
		return Task::whereBetween('start_date',[$start,$end])->get();
	}

	public function setStartDateAttribute($value){

		$this->attributes['start_date'] = Carbon::parse($value)->toDateTimeString();
	}

	public function setEndDateAttribute($value){
		$this->attributes['end_date'] = Carbon::parse($value)->toDateTimeString();
	}

	public function setDueDateAttribute($value){
		$this->attributes['due_date'] = Carbon::parse($value)->toDateTimeString();
	}

	public function getDueDateAttribute($value){

		return Carbon::parse($value)->format('m/d/y');
	}

}

