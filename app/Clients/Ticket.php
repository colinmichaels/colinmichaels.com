<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

use Spatie\Activitylog\Traits\LogsActivity;

class Ticket extends Model implements HasMedia
{
	use HasMediaTrait;
	use LogsActivity;

	protected static $logName = 'ticket';
	protected static $logOnlyDirty = true;
	protected static $logAttributes = ["*"];


	public function getDescriptionForEvent(string $eventName): string
	{
		return "Task: <strong>" .$this->name."</strong>  has been {$eventName}";
	}

	public static function toList(){

		return Project::all()->pluck('name','id')->toArray();

	}

    public function scopeActive($query){

    	$query->whereActive(1);
    }
	public function project() {
		return $this->belongsTo( 'App\Project' );
	}

	public function client(){
		return $this->belongsTo('App\Client');
	}
	public function user(){
		return $this->belongsTo('App\User');
	}

    public function getStatusAttribute(){

    	return $this->hasOne('App\TicketStatus', 'id','status_id')->first();
    }

    public function assignedTo(){
    	return $this->hasOne('App\User', 'id', 'user_id')->first();
    }

    public function getClientNameAttribute(){

    	return $this->hasOne('App\Client', 'id', 'client_id')->first();
    }
}
