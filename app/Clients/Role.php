<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {
	public static function toList() {

		return Role::all()->pluck( 'name', 'id' )->toArray();

	}
}
