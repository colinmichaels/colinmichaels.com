<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

use Carbon;


class Invoice extends Model implements HasMedia {

	use HasMediaTrait;
	use LogsActivity;

	protected static $logName = 'lead';
	protected static $logOnlyDirty = true;
	protected static $logAttributes = ["*"];

	protected $table = 'invoices';
	protected $hidden = ['created_at','updated_at'];
	protected $fillable = [
		'po_number',
		'client_id',
		'project_id',
		'invoice_date',
		'due_date',
		'status_id',
		'total',
		'paid'
	];

	public function getDescriptionForEvent(string $eventName): string
	{
		return "Lead: <strong>" .$this->name."</strong>  has been {$eventName}";
	}

	public function project() {
		return $this->belongsTo( 'App\Project' );
	}

	public function client() {
		return $this->belongsTo( 'App\Client' );
	}

	public function user() {
		return $this->belongsTo( 'App\User' );
	}

	public function getStatusAttribute(){
		return  $this->hasOne('App\InvoiceStatus','id','status_id')->first();
	}

	public function getBalanceAttribute() {

	return $this->paid - $this->total;

	}

	public static function InvoicesTotal() {

		return Invoice::all()->pluck('total')->sum();

	}

	public static function InvoicesDue(){
		$invoices = Invoice::all();
		return $invoices->reduce(function($carry, $invoice){
			return $carry + ($invoice->paid - $invoice->total);
		});

	}

	public static function InvoicesBalance(){

		$invoice = new Invoice;
		return $invoice->InvoicesTotal() - $invoice->InvoicesTotalPaid();

	}

	public static function InvoicesTotalPaid(){
		return Invoice::all()->pluck('paid')->sum();

	}

	public static function scopeInvoicesBetween($query,$start,$end){

		$query->whereBetween('invoice_date',$start,$end);
	}

	public static function InvoicesStats(){

		$invoice = New Invoice;
		$invoices_stats['total'] = $invoice->InvoicesTotal();
		$invoices_stats['paid'] = $invoice->InvoicesTotalPaid();
		$invoices_stats['due'] = $invoice->InvoicesDue();
		$invoices_stats['balance'] = $invoice->InvoicesBalance();
		return $invoices_stats;

	}

	public static function ClientInvoicesStats($client_id){

		$client = Client::whereId($client_id)->first();
		$invoices = $client->invoices;
		$total = $invoices->pluck('total')->sum();
		$paid = $invoices->pluck('paid')->sum();
		$due =  $total - $paid;
		$balance = $due;

		$invoices_stats['total'] = $total;
		$invoices_stats['paid'] = $paid;
		$invoices_stats['due'] = $due;
		$invoices_stats['balance'] = $balance;
		return $invoices_stats;

	}

	public function getInvoiceDateAttribute($value){

		return Carbon::parse($value)->format('m/d/y');
	}
	public function getDueDateAttribute($value){

		return Carbon::parse($value)->format('m/d/y');
	}

	public function setTotalAttribute($value){
		$this->attributes['total'] = unformatMoney( $value);
	}
	public function setPaidAttribute($value){
		$this->attributes['paid'] = unformatMoney( $value);
	}

//	public function setInvoiceDateAttribute($value){
//		$this->attributes['invoice_date'] = Carbon::parse($value)->toDateTimeString();
//	}

}
