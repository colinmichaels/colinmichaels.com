<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;

class ProjectType extends Model
{
	public static function toList(){

		return ProjectType::all()->pluck('name','id')->toArray();

	}
}
