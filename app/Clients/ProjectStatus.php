<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;

class ProjectStatus extends Model
{
   protected $table = 'project_statuses';

   public static function toList(){

   	return ProjectStatus::all()->pluck('name','id')->toArray();

   }
}
