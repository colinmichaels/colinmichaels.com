<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketStatus extends Model
{
	public static function toList(){

		return TicketStatus::all()->pluck('name','id')->toArray();

	}
}
