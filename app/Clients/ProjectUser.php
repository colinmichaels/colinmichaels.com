<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;

class ProjectUser extends Model
{
	protected $table = 'project_users';

	public function user(){

		return $this->hasOne('App\User', 'id','user_id');

	}

}
