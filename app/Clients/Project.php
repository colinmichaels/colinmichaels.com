<?php

namespace App\Clients;

use App\Traits\ImageTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;

use App\Clients\ProjectUser;

use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\SchemaOrg\Schema;

class Project extends Model implements HasMedia  {

	use ImageTrait;
	use LogsActivity;
    private $title,$url,$description;
	protected static $logName = 'project';
	protected static $logOnlyDirty = true;
	protected static $logAttributes = [
		'user_id',
		'client_id',
		'status_id',
		'project_type_id',
		'name',
		'description',
		'hours',
		'active' ,
		'slack_channel'

	];

	protected $fillable = [
		'user_id',
		'client_id',
		'status_id',
		'project_type_id',
		'name',
		'description',
		'hours',
		'active',
		'slack_channel'

	];
	protected $hidden = [
		'created_at',
		'updated_at'
	];

    /**
     * @param mixed $url
     */
    public function setUrl( $url ): void {
        $this->url = filter_var($url,FILTER_SANITIZE_URL);
    }

    public function scopeActive($query){
        return $query->whereActive(1)->get();
    }

    public function getSchema(){
        return  Schema::webSite()
                      ->name($this->title)
                      ->about($this->description)
                      ->copyrightHolder($this->title)
                      ->datePublished($this->date_completed)
                      ->thumbnailUrl($this->thumbnail)
                      ->toScript();

    }


    public function getDescriptionForEvent(string $eventName): string
	{
		return "Project: <strong>" .$this->name."</strong>  has been {$eventName}";
	}

	public static function toList(){

		return Project::all()->pluck('name','id')->toArray();

	}

	public function tasks(){
		return $this->hasMany('App\Task', 'project_id');
	}

	public function totalHours(){

		return array_sum($this->tasks()->pluck('hours')->toArray());

	}

	public function credentials(){
		return $this->hasMany('App\Credential', 'project_id');
	}
	public function owner(){
		return $this->hasOne('App\User','id','user_id');
	}
	public function members(){
		return $this->hasMany('App\ProjectUser','project_id');
	}
	public function client(){
		return $this->belongsTo('App\Client');
	}

	public function type(){
		return $this->hasOne('App\ProjectType', 'id','project_type_id');
	}

	public function status(){
		return $this->hasOne('App\ProjectStatus', 'id','status_id');
	}
//	public function getStatusAttribute(){
//		return  $this->hasOne('App\ProjectStatus','id','status_id')->first()->name;
//	}
//	public function uploads(){
//		return $this->hasMany('App\Upload', 'project_id');
//	}
	/**
	 * Checks if teh currently Auth user
	 * is the owner of the project.
	 *
	 * @return bool
	 */
	public function isOwner(){
		if($this->user_id != Auth::id()){
			return false;
		}
		return true;
	}
	/**
	 * Checks if the current Auth user
	 * is a member of a given project.
	 *
	 * @return bool
	 */
	public function isMember(){
		$s = ProjectUser::whereProjectId($this->id)->whereUserId(Auth::id())->get();
		if(count($s) == 0){
			return false;
		}
		return true;
	}
	// Get the toal weight of the given project
	public function totalWeight(){
		return $this->tasks()->where('state','!=','complete')->sum('weight');
	}
}
