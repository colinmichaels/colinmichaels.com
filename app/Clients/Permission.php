<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;

use Spatie\Activitylog\Traits\LogsActivity;

class Permission extends Model
{
	use LogsActivity;

	protected static $logName = 'permission';
	protected static $logOnlyDirty = true;
	protected static $logAttributes = ["*"];

	public function getDescriptionForEvent(string $eventName): string
	{
		return "Permission: <strong>" .$this->name."</strong>  has been {$eventName}";
	}
}
