<?php

namespace App\Clients;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

use Carbon;

class Website extends Model
{

	use LogsActivity;

	protected static $logName = 'website';
	protected static $logOnlyDirty = true;
	protected static $logAttributes = ["*"];

	protected $hidden = [
		'hosting_password',
		'site_password',
		'dns_password',
		'mail_password'
	];

	protected $fillable= [
		'name',
		'platform',
		'hosting_provider',
		'hosting_username',
		'hosting_password',
		'site_login',
		'site_password',
		'client_id',
		'dns_provider',
		'dns_username',
		'dns_password',
		'mail_provider',
		'mail_login',
		'mail_password',
		'admin_url',
		'production',
		'stage',
		'dev',
		'repository',
		'date_live',
		'notes',
		'has_ssl', 'ip_address'
	];
	protected $casts =[
		'has_ssl' => 'boolean'
		];

	public function getDescriptionForEvent(string $eventName): string
	{
		return "Website: <strong>" .$this->name."</strong>  has been {$eventName}";
	}

    public function client(){
	    return $this->hasOne('App\Client', 'id', 'client_id');
    }

    public function getIsLiveAttribute(){

		return (Carbon::parse($this->date_live)->lte(Carbon::now()));
    }


	public function getDnsProviderLinkAttribute(){

    	switch(strtolower($this->dns_provider)){

		    case('godaddy'):
		    	return 'https://sso.godaddy.com/?realm=idp&path=%2Fproducts&app=account&username='.$this->dns_username;
		    	break;
		    case('cloudflare'):
		    	return   'https://www.cloudflare.com/a/login?email='.$this->dns_username;
		    	break;
		    case('enom'):
			    return   'https://www.enomcentral.com/login.aspx?login_id='.$this->dns_username;
			    break;
		    default:
		    	return null;
		    	break;
	    }
	}

	public function getProductionHostAttribute($url){

    	$base = parse_url($this->production);
    	if(isset($base['host'])) return $base['host'];
    	else return $this->production;
	}

	public function getDateLiveAttribute($value){

		return (!is_null($value))? Carbon::parse($value)->format('m/d/Y'): null;
	}

	public function setDateLiveAttribute($value){

		$this->attributes['date_live'] = Carbon::parse($value)->toDateTimeString();

	}
	public function setHasSslAttribute($value){

		$this->attributes['has_ssl'] = ($value)? true : false;

	}
}
