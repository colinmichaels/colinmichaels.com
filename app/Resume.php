<?php

namespace App;

class Resume
{

    public function build()
    {
        $resume = [
            'education'=> Education::all(),
            'experience' => Experience::all()
        ];
        return json_decode(json_encode($resume),false);
    }

}

class Experience extends Resume
{

    public static function all()
    {
        return [
            [
                'title'   => 'Digital Director',
                'company' => [
                    'title'   => 'OGK Creative',
                    'city'    => 'Delray Beach',
                    'state'   => 'FL',
                    'address' => '1200 E. Atlantic Ave',
                    'zip'     => '33458',
                    'url'     => 'https://ogkcreative.com'
                ],
                'start_date' => 'Dec 2017',
                'end_date' => 'Current',
                'responsibilities' => [
                    'Responsible for the managing of all development projects.',
                    'Managed the development team members.',
                    'Designed and Developed custom application solutions.'
                ]
            ],
            [
                'title' => 'Senior Applications Developer',
                'company' => [
                    'title' => 'ION Media Networks',
                    'city' => 'West Palm Beach',
                    'state' => 'FL',
                    'address' => '601 Clearwater Park Rd',
                    'zip' => '33401',
                    'url' => 'https://ionmedia.com/'
                ],
                'start_date' => 'Feb 2016',
                'end_date' => 'Dec 2017',
                'responsibilities'=>[
                    'Developed public facing web applications for each network.',
                    'Managed web applications cloud network infrastructure.',
                    'Maintained all web content through custom built CMS using Laravel.'
                ]
            ],
            [
                'title' => 'Web Applications Developer',
                'company' => [
                    'title' => '15th Judicial Circuit',
                    'city' => 'West Palm Beach',
                    'state' => 'FL',
                    'address' => '205 North Dixie Hwy.',
                    'zip' => '33401',
                    'url' => 'https://www.15thcircuit.com'
                ],
                'start_date' => 'Sept 2015',
                'end_date' => 'Jan 2016',
                'responsibilities'=>[
                    'Contributed to the development of web applications for the courts.',
                    'Collaborated on multiple projects using version controlled systems.',
                    'Assisted in building an electronic filing system for court documents'
                ]
            ],
            [
                'title' => 'Senior Web Developer',
                'company' => [
                    'title' => 'QuinnCom Communications',
                    'city' => 'Jupiter',
                    'state' => 'FL',
                    'address' => '1155 Main St. #109',
                    'zip' => '33458',
                    'url' => 'http://quinncom.net/'
                ],
                'start_date' => 'Mar 2011',
                'end_date' => 'Sept 2015',
                'responsibilities'=>[
                    'Managed all client sites and servers.',
                    'Developed custom WordPress, OpenCart, Magento, WooCommerce.',
                    'Maintained customer Search Engine Optimization and site performance.'
                ]
            ]
        ];

    }

}

class Education extends Resume
{

    public static function all()
    {
        return [
            [
                'title'       => 'Indian River State College',
                'city'        => 'Ft Pierce',
                'state'       => 'Florida',
                'address'     => '3209 Virginia Avenue',
                'zip'         => '',
                'phone'       => '1-772-462-4772',
                'url'         => 'http://www.irsc.edu/',
                'wiki'        => 'https://en.wikipedia.org/wiki/Indian_River_State_College',
                'start_date'  => 2010,
                'end_date'    => 2012,
                'degree'      => 'AAS, Computer Information Technology',
                'degree-link' => 'https://www.irsc.edu/programs/computer-information-technology.html'
            ],
            [
                'title'       => 'Full Sail University',
                'city'        => 'Winter Park',
                'state'       => 'Florida',
                'address'     => '3300 University Boulevard',
                'zip'         => '32792',
                'phone'       => '1-800-226-7625',
                'url'         => 'https://www.fullsail.edu/',
                'wiki'        => 'https://en.wikipedia.org/wiki/Full_Sail_University',
                'start_date'  => 1998,
                'end_date'    => 1999,
                'degree'      => 'AAS,Recording Arts',
                'degree-link' => 'https://www.fullsail.edu/degrees/recording-arts-bachelor'
            ],
            [
                'title'       => 'Kent State University',
                'city'        => 'Kent',
                'state'       => 'Ohio',
                'address'     => '800 E. Summit St.',
                'zip'         => '44242',
                'phone'       => '1-330-672-3000',
                'url'         => 'https://www.kent.edu/',
                'wiki'        => 'https://en.wikipedia.org/wiki/Kent_State_University',
                'start_date'  => 1996,
                'end_date'    => 1998,
                'degree'      => 'BA, Marketing',
                'degree-link' => 'http://catalog.kent.edu/colleges/bu/mken/marketing-bba/'
            ],

        ];


    }

}
