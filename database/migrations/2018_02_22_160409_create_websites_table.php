<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('websites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('platform')->default('Wordpress');
            $table->string('hosting_provider')->default('WPengine');
            $table->string('hosting_username')->nullable();
	        $table->string('hosting_password')->nullable();
            $table->string('site_login')->nullable();
	        $table->string('site_password',60)->nullable();
            $table->unsignedInteger( 'client_id')->nullable();
            $table->string('dns_provider')->nullable();
            $table->string('dns_username')->nullable();
            $table->string('dns_password')->nullable();
            $table->string('mail_provider')->nullable();
            $table->string('mail_login')->nullable();
            $table->string('mail_password')->nullable();
	        $table->string('admin_url')->nullable();
	        $table->string('production')->nullable();
	        $table->string('stage')->nullable();
	        $table->string('dev')->nullable();
	        $table->string('repository')->nullable();
	        $table->dateTime('date_live')->nullable();
	        $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('websites');
    }
}
