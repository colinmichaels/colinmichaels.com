<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean( 'active')->default(1);
            $table->integer('client_id')->nullable();
	        $table->integer('project_id')->nullable();
            $table->integer('status_id')->default(1);
            $table->integer('taskable_id')->nullable();
	        $table->integer('user_id')->default(1);
            $table->string('url')->nullable();
            $table->text('comment')->nullable();
            $table->dateTime( 'due_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
