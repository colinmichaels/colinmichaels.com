<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaBuysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_buys', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(0);
            $table->string('name')->nullable();
            $table->unsignedSmallInteger( 'status_id')->default(0);
            $table->unsignedInteger('platform_id')->default(0);    //platforms
            $table->unsignedInteger('vendor_id')->default(0);      //vendors
            $table->unsignedInteger( 'client_id')->default(0);
            $table->unsignedInteger( 'project_id')->nullable();
            $table->string('program',300);
            $table->string('product',300);
	        $table->dateTime( 'start_date')->useCurrent();
	        $table->dateTime( 'end_date')->useCurrent();
	        $table->float( 'rate')->default(0.0);
	        $table->unsignedInteger( 'method_id')->nullable();
	        $table->boolean('net_gross')->default(true);
	        $table->unsignedInteger( 'quantity')->default(0);
	        $table->unsignedInteger('net_media_cost')->default(0);
	        $table->unsignedInteger('commision')->default(0);
	        $table->unsignedInteger('additional_cost')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_buys');
    }
}
