<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('New Task');
            $table->text('description')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('department_id')->nullable();
            $table->integer('assigned_by')->nullable();
            $table->integer('assigned_to')->nullable();
            $table->integer('status_id')->nullable();
            $table->integer('priority_id')->default(1);
            $table->dateTime( 'start_date')->useCurrent();
	        $table->dateTime( 'end_date')->useCurrent();
	        $table->dateTime( 'due_date')->useCurrent();
	        $table->float('est_hours')->default(0);
	        $table->float( 'hours')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
