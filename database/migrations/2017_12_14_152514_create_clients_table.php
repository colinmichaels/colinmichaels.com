<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
	        $table->increments('id');
	        $table->boolean('active')->default(true);
	        $table->integer('user_id')->nullable();
	        $table->string('email')->nullable();
	        $table->string('phone')->nullable();
	        $table->string('address')->nullable();
	        $table->string('city')->nullable();
	        $table->string('state')->nullable();
	        $table->string('country')->default('United States');
	        $table->string( 'zip')->nullable();
	        $table->string('name',300);
	        $table->string('client_code')->nullable();
	        $table->text('notes')->nullable();
	        $table->boolean('has_website')->default(false);
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
