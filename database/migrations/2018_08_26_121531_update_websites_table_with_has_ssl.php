<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWebsitesTableWithHasSsl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('websites', function (Blueprint $table) {

		    $table->boolean( 'has_ssl')->default(false);
		    $table->string( 'ip_address')->nullable();
		    $table->string( 'wp_engine_url')->nullable();


	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
