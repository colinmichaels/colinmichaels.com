<?php

use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run(
    )
    {
        DB::table('pages')->insert([
               "title" => config('meta.title'),
                "description" =>config('meta.description'),
                "slug" => str_slug(''),
                "keywords" => config('meta.keywords'),
                "parent_id" => 0
            ]);
    }
}
