<?php

use Illuminate\Database\Seeder;

use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $users = array(
		    ['name' => 'Colin Michaels', 'email' => 'colinmichaels@gmail.com', 'password' => 'Dikia3540']
	    );

	    foreach($users as $user){
		    $newUser = new User;
		    $newUser->name = $user['name'];
		    $newUser->email = $user['email'];
		    $newUser->password = Hash::make($user['password']);
		    $newUser->save();
	    }
    }
}
