<?php

use Illuminate\Database\Seeder;

use App\Project;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $projects = [
		    ["url" => "https://ionmedia.com" , "title" => "ION Media Networks", "desc" =>  "ION Media Corporate Site", "tog" => "lightbox", "img" => "ionmedia.jpg"],
		    ["url" => "https://iontelevision.com" , "title" => "ION Television", "desc" =>  "ION Television", "tog" => "lightbox", "img" => "ion_television.jpg"],
		    ["url" => "https://ionlife.com" , "title" => "ION Life", "desc" =>  "ION Life", "tog" => "lightbox", "img" => "ion_life.jpg"],
		    ["url" => "https://qubo.com" , "title" => "Qubo", "desc" =>  "Qubo Kids Channel", "tog" => "lightbox", "img" => "qubo.jpg"],
		    ["url" => "http://theionequation.com" , "active" => 0, "title" => "The ION Equation", "desc" =>  "2016 Upfront campaign ION Media", "tog" => "lightbox", "img" => "ionequation.jpg"],
		    ["url" => "http://modernjuiceco.com/" , "title" => "Modern Juice Co." , "desc" => "Modern Juice Co. | Cold Pressed Juices","tog" => "lightbox", "img" => "modern.jpg"],
		    ["url" => "http://drinksnowater.com/" , "title" => "SnoWater" , "desc" => "SnoWater","tog" => "lightbox" ,"img" => "sno.jpg"],
		    ["url" => "http://budschicken.com" , "title" => "Buds Chicken" , "desc" => "Buds Chicken","tog" => "lightbox" ,"img" => "buds.jpg"],
		    ["url" => "http://solutechenvironmental.com/ " , "title" => "Solutech Environmental" , "desc" => "Solutech Environmental","tog" => "lightbox" ,"img"=> "solutech.jpg"
		    ],
		    ["url" => "http://turtlebeachconst.com/ " , "title" => "Turtle Beach Construction" , "desc" => "Turtle Beach Construction","tog" => "lightbox" , "img" => "turtle.jpg"],
		    ["url" => "http://revolutiondating.com/ " , "active" => 0,  "title" => "Revolution Dating" , "desc" => "Revolution Dating","tog" => "lightbox" ,"img" => "revo.jpg"],
		    ["url" => "http://macqueenair.com/ " , "active" => 0,  "title" => "MacQueen AirConditioning" , "desc" => "MacQueen AirConditioning","tog" => "lightbox","img" => "macqueen.jpg"],
		    ["url" => "http://canyongear.com/orderform/" ,   "active" => 0, "title" => "Canyongear" , "desc" => "Custom Sales Order Form","tog" => "lightbox" , "img" => "canyon.jpg"],
		    ["url" => "http://www.tysonlawgroup.com/" ,   "active" => 0, "title" => "Tyson Law Group" , "desc" => "Tyson Law Group","tog" => "lightbox" , "img" => "tyson.jpg"],
		    ["url" => "http://colinmichaels.com/halloween/" , "title" => "Halloween Game" , "desc" => "Halloween Game - HTML5 Canavas", "img" =>"halloween.jpg" ,"tog" => "lightbox"],
		    ["url" => "http://colinmichaels.com/old_files/" , "title" => "My Old Site" , "desc" => "My Old Site Circa: 2004","tog" => "lightbox" ,"img" => "colin.jpg"]
	    ];
	    $imagePath = asset('/images/screenshots/');

	    foreach($projects as $project){
		    $newProject = new Project;
		    $newProject->active = ($project['active'])?? 1;
		    $newProject->title = $project['title'];
		    $newProject->url = $project['url'];
		    $newProject->description = $project['desc'];
		    $newProject->addMediaFromUrl($imagePath.'/'.$project['img'])->toMediaCollection('images');
		    $newProject->save();
	    }
    }
}
